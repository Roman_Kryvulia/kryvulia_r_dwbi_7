﻿USE [R_Kryvulia_Library]
GO

--CREATE SEQUENCE  gen_number AS INT START WITH 10 INCREMENT BY 2;  
--GO 
--next value for gen_number


INSERT INTO [author](author_id, name, birthday, book_amount, issue_amount, total_edition) VALUES 
(next value for gen_number, 'karpa_irena', '1980-12-08', 88, 420, 16850),
(next value for gen_number, 'semeniuk_sviatoslav', '1948-08-07', 34, 226, 778430),
(next value for gen_number, 'nestajko_vsevolod', '1930-01-30', 112, 769, 627562),
(next value for gen_number, 'zubchenko_oleksandr', '1955-09-04', 82, 540, 539960),
(next value for gen_number, 'dahl_roald', '1916-09-13', 152, 425, 297855),
(next value for gen_number, 'travers_pamela', '1899-09-13', 57, 202, 167840),
(next value for gen_number, 'coolidge_susan', '1835-01-29', 17, 93, 100840),
(next value for gen_number, 'bachynskiy_andriy', '1968-01-19', 33, 150, 90450),
(next value for gen_number, 'fine_anne', '1947-12-07', 249, 520, 250320),
(next value for gen_number, 'noestlinger_christine', '1936-10-13', 230, 770, 400770),
(next value for gen_number, 'strong_jeremi', '1949-11-18', 154, 320, 132560),
(next value for gen_number, 'gehm_franziska', '1974-01-01', 454, 1320, 532550),
(next value for gen_number, 'volkov_oleksander', '1891-08-14', 625, 5120, 1932550),
(next value for gen_number, 'vdovychenko_halyna', '1959-06-21', 44, 120, 136500),
(next value for gen_number, 'gretkowska_manuela', '1964-11-06', 39, 90, 122700),
(next value for gen_number, 'matios_mariia', '1959-12-19', 43, 112, 205900),
(next value for gen_number, 'frankiv_liubomyr', '1945-01-09', 25, 88, 169900),
(next value for gen_number, 'kokotiukha_andriy', '1970-11-17', 57, 125, 456700),
(next value for gen_number, 'kononenko_yevgeniya', '1959-02-17', 49, 95, 253200),
(next value for gen_number, 'sabatini_rafael', '1875-04-29', 37, 85, 397500),
(next value for gen_number, 'deresh_liubko', '1984-07-03', 25, 47, 193300),
(next value for gen_number, 'lapikura_valeriy', '1943-09-09', 54, 119, 322500),
(next value for gen_number, 'lapikura_natalia', '1951-03-01', 54, 119, 422500),
(next value for gen_number, 'denysenko_larysa', '1973-06-17', 40, 85, 323500)
GO



INSERT INTO [publisher] (publisher_id, name, URL, country, city, book_amount, issue_amount, total_edition) VALUES 
(next value for gen_number, 'Клуб сімейного дозвілля', 'www.trade.bookclub.ua', 'ukraine', 'kharkiv', 3806, 15555, 3090875),
(next value for gen_number, 'Фоліо', 'www.folio.com.ua', 'ukraine', 'kharkiv', 3806, 15555, 3090875),
(next value for gen_number, 'Апріорі', 'www.apriori.com.ua', 'ukraine', 'lviv', 1712, 9567, 2700859),
(next value for gen_number, 'Видавництво Старого Лева', 'www.starylev.com.ua', 'ukraine', 'lviv', 5845, 22557, 6090643),
(next value for gen_number, 'Махаон-Україна', 'www. machaon.kiev.ua', 'ukraine', 'kyiv', 6739, 23790, 4906721),
(next value for gen_number, 'Урбіно', 'www.urbino.com.ua', 'ukraine', 'lviv', 4402, 16593, 2990445),
(next value for gen_number, 'Mikko', 'www.mikko.com.ua', 'ukraine', 'kharkiv', 1903, 31535, 2790474),
(next value for gen_number, 'Нора-друк', 'www.nora-druk.com', 'ukraine', 'kyiv', 2809, 35555, 5090875),
(next value for gen_number, 'Піраміда', 'www.puramida.net.ua', 'ukraine', 'lviv', 7311, 24498, 3028640),
(next value for gen_number, 'Каменяр', 'www.kameniar.com', 'ukraine', 'lviv', 12306, 215435, 1286309),
(next value for gen_number, 'Майдан', 'www.majdan.net.ua', 'ukraine', 'kyiv', 796, 13875, 890309),
(next value for gen_number, 'Грані-Т', 'www.grani-t.com.ua', 'ukraine', 'kyiv', 2209, 23150, 35090300)
GO

UPDATE [publisher]
SET [URL] = 'www.ababahalamaga.com.ua' WHERE [name] = 'a-ba-ba-ga-la-ma-ga'
GO
UPDATE [publisher]
SET [URL] = 'www.calvaria.org' WHERE [name] = 'kalvaria'
GO

INSERT INTO [book] (ISBN, publisher_id, URL, price, title, published)  VALUES 
('978-966-14-0807-3', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Клуб сімейного дозвілля'),
 'www.zhovta_knyga.com.ua', 55,'Жовта книга', '2010-01-01'),
 ('966-03-3603-9', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Фоліо'),
 'www.khuligany.com.ua', 90,'Хулігани', '2006-01-01'),
  ('978-966-14-0364-1', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Клуб сімейного дозвілля'),
 'www.kobzar.com.ua', 350,'Кобзар', '2010-01-01'),
 ('978-966-2154-14-6', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Апріорі'),
 'www.history_ukr_people.com.ua', 900,'Історія ураїнського народу', '2010-01-01'),
  ('978-966-7047-86-3', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'a-ba-ba-ga-la-ma-ga'),
 'www.toreadory.com.ua', 120,'Тореадори з Васюківки', '2016-01-01'),
  ('978-617-679-349-6', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Видавництво Старого Лева'),
 'www.peremagayuchy_doliu.com.ua', 120,'Перемагаючи долю', '2017-01-01'),
 ('978-617-585-054-1', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'a-ba-ba-ga-la-ma-ga'),
 'www.VDV.com.ua', 120,'ВДВ', '2016-01-01'),
  ('978-617-7200-86-3', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Махаон-Україна'),
 'www.Emil_from_Lioneberg.com.ua', 187,'Пригоди Еміля з Льонеберга', '2015-01-01'),
  ('978-617-7200-64-1', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Махаон-Україна'),
 'www.M_Poppins.com.ua', 135,'Меррі Поппінс', '2014-01-01'),
   ('978-966-917-046-0', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Махаон-Україна'),
 'www.M_Poppins_park.com.ua', 150,'Меррі Поппінс у парку', '2016-01-01'),
  ('978-966-917-041-5', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Махаон-Україна'),
 'www.M_Poppins_door.com.ua', 140,'Меррі Поппінс відчиняє двері', '2015-01-01'),
   ('978-966-917-094-1', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Махаон-Україна'),
 'www.M_Poppins_a.com.ua', 120,'Меррі Поппінс від А до Я', '2016-01-01'),
 ('978-966-917-040-8', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Махаон-Україна'),
 'www.M_Poppins_comeback.com.ua', 180,'Меррі Поппінс повертається', '2015-01-01'),
 ('978-617-679-046-4', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Видавництво Старого Лева'),
 'www.Nevg_keity.com.ua', 115,'Невгамовна Кейті', '2014-01-01'),
  ('978-617-679-074-7', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Видавництво Старого Лева'),
 'www.Detektyvy_v_Arteku.com.ua', 75,'Детективи в Артеку', '2016-01-01'),
  ('978-617-679-343-4', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Видавництво Старого Лева'),
 'www.Pozadu_liodovni.com.ua', 130,'Позаду льодовні', '2016-01-01'),
 ('978-966-2647-03-7', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Урбіно'),
 'www.Margarytko.com.ua', 150,'Маргаритко, моя квітко', '2016-01-01'),
   ('966-8476-04-2', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Видавництво Старого Лева'),
 'www.rockets4.com.ua', 139,'Ракета на чотирьох лапах', '2015-01-01'),
 ('978-617-588-067-8', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Mikko'),
 'www.sestry_vamp.com.ua', 160,'Сестри-вампірки', '2014-01-01'),
  ('978-617-526-224-5', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Махаон-Україна'),
 'www.7koroliv.com.ua', 180,'Сім підземних королів', '2010-01-01'),
 ('978-617-679-129-4', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Видавництво Старого Лева'),
 'www.36and6_cats.com.ua', 139,'36 і 6 котів', '2015-01-01'),
 ('966-8321-76-6', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Нора-друк'),
 'www.Polka.com.ua', 160,'Полька', '2005-01-01'),
  ('966-7188-98-1', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Піраміда'),
 'www.Sweet_Darusia.com.ua', 125,'Солодка Даруся', '2004-01-01'),
    ('5-7745-1012-3', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Каменяр'),
 'www.priton.com.ua', 69,'Притон', '2004-01-01'),
 ('978-966-2961-39-3', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Нора-друк'),
 'www.anomalna_zona.com.ua', 80,'Аномальна зона', '2009-01-01'),
  ('966-663-101-6', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'kalvaria'),
 'www.Poviji.com.ua', 95,'Повії теж виходять заміж', '2004-01-01'),
    ('966-963-035-4', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'kalvaria'),
 'www.escort.com.ua', 69,'Ескорт у смерть', '2002-01-01'),
   ('966-03-3031-6', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Фоліо'),
 'www.zaboroneni_igry.com.ua', 95,'Заборонені ігри', '2005-01-01'),
   ('966-663-143-1', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'kalvaria'),
 'www.kult.com.ua', 55,'Культ', '2004-01-01'),
  ('966-2961-03-8', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Нора-друк'),
 'www.neposydiuchi_pokijnyky.com.ua', 77,'Непосидючі покійники', '2006-01-01'),
   ('978-966-2647-06-8', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Урбіно'),
 'www.golova_minotavra.com.ua', 55,'Голова мінотавра', '2012-01-01')
GO
INSERT INTO [book] (ISBN, publisher_id, URL, price, title, published,edition, issue)  VALUES 
 ('978-966-2923-36-0', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Грані-Т'),
 'www.liza_and_ciucia_P.com.ua', 77,'Ліза та цюця П.', '2016-01-01', 7000, 2),
 ('978-966-14-0364-1', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Урбіно'),
 'www.kobzar_U.com.ua', 320,'Кобзар', '2015-01-01',2000, 100),
 ('978-966-14-0364-1', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Урбіно'),
 'www.kobzar_U2.com.ua', 320,'Кобзар', '2016-01-01',5000, 101),
 ('5-7707-2062-Х', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Майдан'),
 'www.captain_blad.com.ua', 80,'Одісея капітана Блада', '1992-01-01',22644, 5)
GO


UPDATE [book]
SET 
[edition] = 20*(7*publisher_id+(price*(SELECT RAND() Random_Number))),
[issue] = (SELECT ROUND((publisher_id/30),0))
WHERE edition = 1
GO


INSERT INTO [book_author] (book_author_id, ISBN, author_id, seq_no, issue)  VALUES
(71, '978-966-14-0807-3', (SELECT [author_id] FROM [author] WHERE [name] = 'karpa_irena'), 5,
 (SELECT [issue] FROM [book] WHERE [URL] = 'www.zhovta_knyga.com.ua')),
(72, '966-03-3603-9', (SELECT [author_id] FROM [author] WHERE [name] = 'pokalchuk_yurij'), 10,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.khuligany.com.ua')),
(73, '978-966-14-0364-1', (SELECT [author_id] FROM [author] WHERE [name] = 'shevchenko_taras'), 8,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.kobzar.com.ua')),
(74, '978-966-2154-14-6', (SELECT [author_id] FROM [author] WHERE [name] = 'semeniuk_sviatoslav'), 2,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.history_ukr_people.com.ua')),
(75, '978-966-7047-86-3', (SELECT [author_id] FROM [author] WHERE [name] = 'nestajko_vsevolod'), 3,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.toreadory.com.ua')),
(76, '978-617-679-349-6', (SELECT [author_id] FROM [author] WHERE [name] = 'zubchenko_oleksandr'), 5,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.peremagayuchy_doliu.com.ua')),
(77, '978-617-585-054-1', (SELECT [author_id] FROM [author] WHERE [name] = 'dahl_roald'), 5,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.VDV.com.ua')),
(78, '978-617-7200-86-3', (SELECT [author_id] FROM [author] WHERE [name] = 'lindgren_astrid'), 4,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.Emil_from_Lioneberg.com.ua')),
(79, '978-617-7200-64-1', (SELECT [author_id] FROM [author] WHERE [name] = 'travers_pamela'), 2,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.M_Poppins.com.ua')),
(80, '978-966-917-046-0', (SELECT [author_id] FROM [author] WHERE [name] = 'travers_pamela'), 5,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.M_Poppins_park.com.ua')),
(81, '978-966-917-041-5', (SELECT [author_id] FROM [author] WHERE [name] = 'travers_pamela'), 6,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.M_Poppins_door.com.ua')),
(82, '978-966-917-094-1', (SELECT [author_id] FROM [author] WHERE [name] = 'travers_pamela'), 7,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.M_Poppins_a.com.ua')),
(83, '978-966-917-040-8', (SELECT [author_id] FROM [author] WHERE [name] = 'travers_pamela'), 4,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.M_Poppins_comeback.com.ua')),
(84, '978-617-679-046-4', (SELECT [author_id] FROM [author] WHERE [name] = 'coolidge_susan'), 7,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.Nevg_keity.com.ua')),
(85, '978-617-679-074-7', (SELECT [author_id] FROM [author] WHERE [name] = 'bachynskiy_andriy'), 4,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.Detektyvy_v_Arteku.com.ua')),
(86, '978-617-679-343-4', (SELECT [author_id] FROM [author] WHERE [name] = 'fine_anne'), 5,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.Pozadu_liodovni.com.ua')),
(87, '978-966-2647-03-7', (SELECT [author_id] FROM [author] WHERE [name] = 'noestlinger_christine'), 3,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.Margarytko.com.ua')),
(88, '966-8476-04-2', (SELECT [author_id] FROM [author] WHERE [name] = 'strong_jeremi'), 4,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.rockets4.com.ua')),
(90, '978-617-588-067-8', (SELECT [author_id] FROM [author] WHERE [name] = 'gehm_franziska'), 5,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.sestry_vamp.com.ua')),
(91, '978-617-526-224-5', (SELECT [author_id] FROM [author] WHERE [name] = 'volkov_oleksander'), 3,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.7koroliv.com.ua')),
(92, '978-617-679-129-4', (SELECT [author_id] FROM [author] WHERE [name] = 'vdovychenko_halyna'), 4,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.36and6_cats.com.ua')),
(93, '966-8321-76-6', (SELECT [author_id] FROM [author] WHERE [name] = 'gretkowska_manuela'), 5,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.Polka.com.ua')),
(94, '966-7188-98-1', (SELECT [author_id] FROM [author] WHERE [name] = 'matios_mariia'), 3,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.Sweet_Darusia.com.ua')),
(95, '5-7745-1012-3', (SELECT [author_id] FROM [author] WHERE [name] = 'frankiv_liubomyr'), 4,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.priton.com.ua')),
(96, '978-966-2961-39-3', (SELECT [author_id] FROM [author] WHERE [name] = 'kokotiukha_andriy'), 5,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.anomalna_zona.com.ua')),
(97, '966-663-101-6', (SELECT [author_id] FROM [author] WHERE [name] = 'kononenko_yevgeniya'), 6,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.Poviji.com.ua')),
(98, '966-963-035-4', (SELECT [author_id] FROM [author] WHERE [name] = 'rozdobudko_iren'), 1,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.escort.com.ua')),
(99, '5-7707-2062-Х', (SELECT [author_id] FROM [author] WHERE [name] = 'sabatini_rafael'), 2,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.captain_blad.com.ua')),
(100, '966-03-3031-6', (SELECT [author_id] FROM [author] WHERE [name] = 'pokalchuk_yurij'), 1,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.zaboroneni_igry.com.ua')),
(101, '966-663-143-1', (SELECT [author_id] FROM [author] WHERE [name] = 'deresh_liubko'), 2,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.kult.com.ua')),
(102, '966-2961-03-8', (SELECT [author_id] FROM [author] WHERE [name] = 'lapikura_valeriy'), 1,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.neposydiuchi_pokijnyky.com.ua')),
(103, '966-2961-03-8', (SELECT [author_id] FROM [author] WHERE [name] = 'lapikura_natalia'), 1,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.neposydiuchi_pokijnyky.com.ua')),
(104, '978-966-2647-06-8', (SELECT [author_id] FROM [author] WHERE [name] = 'krajewski_marek'), 1,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.golova_minotavra.com.ua')),
(105, '978-966-2923-36-0', (SELECT [author_id] FROM [author] WHERE [name] = 'denysenko_larysa'), 5, 
(SELECT [issue] FROM [book] WHERE [URL] = 'www.liza_and_ciucia_P.com.ua'))
GO
ALTER TABLE [book_author]
DROP UQ_ISBN_author_id
GO
INSERT INTO [book_author] (book_author_id, ISBN, author_id, seq_no, issue)  VALUES
(106, '978-966-14-0364-1', (SELECT [author_id] FROM [author] WHERE [name] = 'shevchenko_taras'), 6, 
(SELECT [issue] FROM [book] WHERE [URL] = 'www.kobzar_U.com.ua')),
(107, '978-966-14-0364-1', (SELECT [author_id] FROM [author] WHERE [name] = 'shevchenko_taras'), 7, 
(SELECT [issue] FROM [book] WHERE [URL] = 'www.kobzar_U2.com.ua'))
GO

--SELECT * FROM [author]
--SELECT * FROM [publisher]
--SELECT * FROM [book]
--SELECT * FROM [book_author]
--SELECT * FROM [author_log]
--GO




--one book - two publisher
SELECT ISBN, title, COUNT(DISTINCT[publisher_id]) [кількість видавців], COUNT([publisher_id]) [кількість видань]  FROM [book]
GROUP BY [ISBN], [title]
HAVING COUNT ([ISBN]) >1
GO

--one book - many edition
SELECT ISBN, title, COUNT(DISTINCT [edition]) [кількість різних тиражів]  FROM [book]
GROUP BY [ISBN], [title]
HAVING COUNT ([ISBN]) >1
GO
--one book - two author
SELECT [book_author].[ISBN], [book].[title],  COUNT (DISTINCT[book_author].[author_id])[кількість авторів] FROM [book_author]
INNER JOIN [book] ON [book_author].[ISBN] = [book].[ISBN]
GROUP BY [book_author].[ISBN], [book].[title]
HAVING COUNT (DISTINCT[book_author].[author_id]) >1
GO
