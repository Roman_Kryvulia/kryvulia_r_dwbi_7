USE [R_Kryvulia_Library]
GO


ALTER trigger [tr_authorIUD_ON_author_log] ON [author]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation CHAR(1)
DECLARE @ins INT = (SELECT COUNT(*) FROM INSERTED)
DECLARE @del INT = (SELECT COUNT(*) FROM DELETED)
SET @operation = 
CASE 
	WHEN @ins > 0 AND @del > 0 THEN 'U'  
	WHEN @ins = 0 AND @del > 0 THEN 'D'  
	WHEN @ins > 0 AND @del = 0 THEN 'I'  
END
---- insert
IF @operation = 'I'
BEGIN

	INSERT INTO [author_log]
            (author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type,
			book_amount_old, issue_amount_old, total_edition_old, book_amount_new, issue_amount_new, total_edition_new)
	SELECT 
		inserted.author_id, 
		inserted.name,
		inserted.URL,
		NULL,
		NULL,
		NULL,
		@operation,
		NULL,
		NULL,
		NULL,
		inserted.book_amount,
		inserted.issue_amount,
		inserted.total_edition
		FROM [author]
	INNER JOIN inserted ON [author].author_id = inserted.author_id
END

---- delete
IF @operation = 'D'
BEGIN
	INSERT INTO [author_log]
	(author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type,
	book_amount_old, issue_amount_old, total_edition_old, book_amount_new, issue_amount_new, total_edition_new)
	SELECT
		NULL,
		NULL,
		NULL,
		deleted.author_id, 
		deleted.name,
		deleted.URL,
		@operation,
		deleted.book_amount,
		deleted.issue_amount,
		deleted.total_edition,
		NULL,
		NULL,
		NULL
	FROM deleted 
END
--- update (+update table [author])
IF @operation = 'U'
BEGIN
	 UPDATE [author]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE author.author_id = inserted.author_id
 			INSERT INTO [author_log]
			(author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type,
			book_amount_old, issue_amount_old, total_edition_old, book_amount_new, issue_amount_new, total_edition_new)
			SELECT
		    inserted.author_id, 
		    inserted.name,
		    inserted.URL,
			deleted.author_id, 
		    deleted.name,
		    deleted.URL,
		    @operation,
			deleted.book_amount,
		    deleted.issue_amount,
		    deleted.total_edition,
			inserted.book_amount,
		    inserted.issue_amount,
		    inserted.total_edition
						FROM [author]
				 INNER JOIN  inserted ON [author].author_id = inserted.author_id
				 INNER JOIN deleted ON [author].author_id = deleted.author_id
				 			 
	END

END
GO