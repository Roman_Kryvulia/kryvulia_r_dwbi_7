﻿USE [R_Kryvulia_Library]
GO
CREATE VIEW [vw_book-publisher]
AS
SELECT [book].[title], [publisher].[name] [видавництво], [book].[edition] [тираж книги], total_edition [сумарний тираж видавництва] FROM [book]
INNER JOIN [publisher] ON [book].[publisher_id] = [publisher].[publisher_id]
WHERE [book].[publisher_id] = 160 
GO
CREATE VIEW [vw_publisher]
AS
 SELECT [name], [book_amount] [кількість книг, виданих видавництвом],
[issue_amount] [кількість видань книг, виданих видавництвом],
[total_edition] [сумарний тираж книг видавництва]
FROM [publisher]
WHERE [publisher_id] = 160
GO 

--check trigger [trg_for_book]

--check trigger on update
-- (тираж книги - 7000, сумарний тираж видавництва-35097300)
SELECT * FROM [vw_book-publisher]
GO

UPDATE [book]
SET [edition] = [edition]+500
WHERE publisher_id = 160 
GO
--(тираж книги - 7000+500=7500, сумарний тираж видавництва-35097300+500=35097800)
SELECT * FROM [vw_book-publisher]
GO

--check trigger on insert (insert new book with issue=1000)

--[кількість книг, виданих видавництвом] = 2210
--[кількість видань книг, виданих видавництвом] =23151
--[сумарний тираж книг видавництва] = 35097800
SELECT * FROM [vw_publisher]

INSERT INTO [book] (ISBN, publisher_id, URL, price, title, published, edition, issue)  VALUES  
('978-966-2923-36-0-2', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Грані-Т'),
 'www.liza_and_ciucia_P-2.com.ua', 177,'Ліза та цюця П.-2', '2016-01-01', 1000, 2)
 GO
--[кількість книг, виданих видавництвом] = 2210+1=2211
--[кількість видань книг, виданих видавництвом] =23150+1=23152
--[сумарний тираж книг видавництва] = 35097800+1000=35098800
SELECT * FROM [vw_publisher]
GO
 --check trigger on delete
 -- delete new book
 DELETE FROM [book]
 WHERE [ISBN] ='978-966-2923-36-0-2' 

--[кількість книг, виданих видавництвом] = 2211-1=2210
--[кількість видань книг, виданих видавництвом] =23152-1=23151
--[сумарний тираж книг видавництва] = 35098800-1000=35097800
SELECT * FROM [vw_publisher]
GO