USE [R_Kryvulia_Library]
GO
--update table [author]
UPDATE [author] 
SET [birthday] ='1854-10-16', [book_amount] = 127, [issue_amount] = 550,  [total_edition] = 250000,
[name] = 'waild_oscar'
WHERE name ='waild'
UPDATE [author]
SET [birthday] ='1814-03-09', [book_amount] = 780, [issue_amount] = 2254,  [total_edition] = 4730124,
[name] ='shevchenko_taras'
WHERE name ='shevchenko'
UPDATE [author]
SET[birthday] ='1904-08-03', [book_amount] = 420, [issue_amount] = 1320,  [total_edition] = 190680,
[name] ='saimak_cliford'
WHERE name ='saimak'
UPDATE [author]
SET [birthday] ='1856-08-27', [book_amount] = 655, [issue_amount] = 1840,  [total_edition] = 1900000,
[name] ='franko_ivan'
WHERE name ='franko'
UPDATE [author] 
SET [birthday] ='1835-11-30', [book_amount] = 750, [issue_amount] = 2350,  [total_edition] = 3450000,
[name] ='twen_mark'
WHERE name ='twen'
UPDATE [author] 
SET [birthday] ='1920-01-02', [book_amount] = 990, [issue_amount] = 3590,  [total_edition] = 6504280,
[name] ='azimov_isaac'
WHERE name ='azimov'
UPDATE [author] 
SET [birthday] ='1859-05-22', [book_amount] = 253, [issue_amount] = 1070,  [total_edition] = 4072860,
[name] ='conan-doil_arthur'
WHERE name ='conan-doil'
UPDATE [author] 
SET [birthday] ='1920-08-22', [book_amount] = 498, [issue_amount] = 2570,  [total_edition] = 7550000,
[name] ='bredberi_rej'
WHERE name ='bredberi'
UPDATE [author]
SET [birthday] ='1941-01-22', [book_amount] = 320, [issue_amount] = 1400,  [total_edition] = 980000,
[name] ='pokalchuk_yurij'
WHERE name ='pokalchuk'
UPDATE [author]
SET [birthday] ='1921-09-12', [book_amount] = 590, [issue_amount] = 2340,  [total_edition] = 6900000,
[name] ='lem_stanislaw'
WHERE name ='lem'
UPDATE [author] 
SET [birthday] ='1974-08-23', [book_amount] = 290, [issue_amount] = 750,  [total_edition] = 459000,
[name] ='zhadan_ihor'
WHERE name ='zhadan'
UPDATE [author] 
SET [birthday] ='1966-09-04', [book_amount] = 328, [issue_amount] = 1497,  [total_edition] = 1985000,
[name] ='krajewski_marek'
WHERE name ='krajewski'
UPDATE [author] 
SET [birthday] ='1891-05-15', [book_amount] = 666, [issue_amount] = 3590,  [total_edition] = 4790000,
[name] ='bulhakov_mykhail'
WHERE name ='bulhakov'
UPDATE [author] 
SET [birthday] ='1907-11-14', [book_amount] = 890, [issue_amount] = 5570,  [total_edition] = 9500000,
[name] ='lindgren_astrid'
WHERE name ='lindgren'
UPDATE [author]
SET [birthday] ='1962-11-03', [book_amount] = 370, [issue_amount] = 1650,  [total_edition] = 1500000,
[name] ='rozdobudko_iren'
WHERE name ='rozdobudko'
GO
--update table [publisher]
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'kyiv', [book_amount] = 1200, [issue_amount] = 2250,  [total_edition] = 705600 WHERE name ='caravela'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'kharkiv', [book_amount] = 450, [issue_amount] = 1900,  [total_edition] = 340530 WHERE name ='new time'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'lviv', [book_amount] = 2476, [issue_amount] = 7557,  [total_edition] = 4380545 WHERE name ='terra incognita'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'lviv', [book_amount] = 690, [issue_amount] = 1756,  [total_edition] = 926903 WHERE name ='intelect'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'kyiv', [book_amount] = 2575, [issue_amount] = 7950,  [total_edition] = 12200000 WHERE name ='a-ba-ba-ga-la-ma-ga'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'ivano-frankivsk', [book_amount] = 200, [issue_amount] = 750,  [total_edition] = 170000 WHERE name ='apostol'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'lviv', [book_amount] = 3000, [issue_amount] = 9250,  [total_edition] = 19080015 WHERE name ='kalvaria'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'zaporizhzhya', [book_amount] = 190, [issue_amount] = 1498,  [total_edition] = 2100670 WHERE name ='inter-book'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'uzhhorod', [book_amount] = 3893, [issue_amount] = 12430,  [total_edition] = 4705520 WHERE name ='karpaty'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'kyiv', [book_amount] = 667, [issue_amount] = 4250,  [total_edition] = 804810 WHERE name ='knyga plus'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'stryj', [book_amount] = 890, [issue_amount] = 6395,  [total_edition] = 2427050 WHERE name ='ukrpol'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'kyiv', [book_amount] = 798, [issue_amount] = 4250,  [total_edition] = 320640 WHERE name ='lira-K'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'kyiv', [book_amount] = 2794, [issue_amount] = 7244,  [total_edition] = 3703025 WHERE name ='klio'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'kyiv', [book_amount] = 850, [issue_amount] = 3250,  [total_edition] = 1250032 WHERE name ='litera LTD'
GO
UPDATE [publisher]
SET [country] ='ukraine', [city] = 'lviv', [book_amount] = 5200, [issue_amount] = 12450,  [total_edition] = 6704305 WHERE name ='meta'
GO

--update table [book]
UPDATE [book]
SET 
[edition] = 20*(7*publisher_id+(price*(SELECT RAND() Random_Number))),
[issue] = (SELECT ROUND((publisher_id/30),0)),
[book].published = [pub].published
FROM [book]
inner join (SELECT [book].ISBN, 
(SELECT CAST((Select DateAdd(Day, Rand() * DateDiff(Day, (DATEADD(year, 30, birthday)) , getdate()), (DATEADD(year, 20, birthday))))AS DATE)) published 
FROM [book_author] JOIN [author] ON [author].author_id = [book_author].author_id
                   JOIN [book] ON [book_author].ISBN=[book].ISBN)
AS [pub]
ON [book].ISBN  = [pub].ISBN
WHERE publisher_id !> 68
GO

UPDATE [book]
SET 
[edition] = 60*(5*publisher_id+(price*(SELECT RAND() Random_Number))),
 [issue] = (SELECT ROUND((3*RAND()+0.05*publisher_id),0)),
 [book].published = [pub].published
FROM [book]
inner join (SELECT [book].ISBN, 
(SELECT CAST((Select DateAdd(Day, Rand() * DateDiff(Day, (DATEADD(year, 30, birthday)) , getdate()), (DATEADD(year, 20, birthday))))AS DATE)) published 
FROM [book_author] JOIN [author] ON [author].author_id = [book_author].author_id
                   JOIN [book] ON [book_author].ISBN=[book].ISBN)
AS [pub]
ON [book].ISBN  = [pub].ISBN
WHERE publisher_id !< 68
GO

UPDATE [book]
SET
 published = (SELECT DATEADD(year, -7, (SELECT CAST (getdate() AS DATE))))
 WHERE published IS NULL
GO

--SELECT * FROM [author]
--SELECT * FROM [author_log]
--SELECT * FROM [publisher]
--SELECT * FROM [book]
--SELECT * FROM [book_author]