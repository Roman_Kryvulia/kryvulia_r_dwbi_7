﻿USE [R_Kryvulia_Library]
GO
DROP TRIGGER IF EXISTS [trg_book_to_publisher]
GO

CREATE TRIGGER [trg_book_to_publisher]
ON [Book]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
SET @operation = 
CASE 
	WHEN @ins > 0 and @del > 0 THEN 'U'  
	WHEN @ins = 0 and @del > 0 THEN 'D'  
	WHEN @ins > 0 and @del = 0 THEN 'I'  
END


IF @operation = 'I'
BEGIN

UPDATE [publisher]
SET 
 [publisher].[book_amount] = [publisher].[book_amount] + [add_table_ins].[кількість книг],
 [publisher].[issue_amount] = [publisher].[issue_amount] + [add_table_ins].[кількість видань],
 [publisher].[total_edition] = [publisher].[total_edition] + [add_table_ins].[тираж]
 FROM [publisher]
 inner join   
 (SELECT  DISTINCT publisher_id, COUNT (publisher_id) [кількість книг], COUNT(issue) [кількість видань], SUM (edition) [тираж]
 FROM [inserted]
 GROUP BY [publisher_id])
AS [add_table_ins]
ON  [publisher].[publisher_Id]= [add_table_ins].[publisher_Id] 
END

IF @operation = 'D'
BEGIN

UPDATE [publisher]
SET 
 [publisher].[book_amount] = [publisher].[book_amount] - [add_table_del].[кількість книг],
 [publisher].[issue_amount] = [publisher].[issue_amount] - [add_table_del].[кількість видань],
 [publisher].[total_edition] = [publisher].[total_edition] - [add_table_del].[тираж]
 FROM [publisher]
 inner join 
 (SELECT  DISTINCT publisher_id, COUNT (publisher_id) [кількість книг], COUNT(issue) [кількість видань], SUM (edition) [тираж]
              FROM [deleted]
			  GROUP BY [publisher_Id])
			  AS [add_table_del]
			  ON  [publisher].[publisher_Id]= [add_table_del].[publisher_Id] 
END

IF @operation = 'U'
BEGIN

UPDATE [publisher]
SET 
 [publisher].[book_amount] = [publisher].[book_amount] + [add_table_ins].[кількість книг]-[add_table_del].[кількість книг],
 [publisher].[issue_amount] = [publisher].[issue_amount] + [add_table_ins].[кількість видань]-[add_table_del].[кількість видань],
 [publisher].[total_edition] = [publisher].[total_edition] + [add_table_ins].[тираж]-[add_table_del].[тираж]
 FROM [publisher]
  INNER JOIN 
   (SELECT  DISTINCT publisher_id, COUNT (publisher_id) [кількість книг], COUNT(issue) [кількість видань], SUM (edition) [тираж]
 FROM [inserted]
 GROUP BY [publisher_id]) AS [add_table_ins]
  ON  [publisher].[publisher_Id]= [add_table_ins].[publisher_Id] 
  INNER JOIN
   (SELECT  DISTINCT publisher_id, COUNT (publisher_id) [кількість книг], COUNT(issue) [кількість видань], SUM (edition) [тираж]
              FROM [deleted]
			  GROUP BY [publisher_Id]) AS [add_table_del]
   ON  [publisher].[publisher_Id]= [add_table_del].[publisher_Id] 
 END
END

