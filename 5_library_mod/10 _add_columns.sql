USE [R_Kryvulia_Library]
GO

ALTER TABLE [author]
ADD birthday DATE NULL,
book_amount INT NOT NULL DEFAULT (0) check (book_amount >= 0),
issue_amount INT NOT NULL DEFAULT (0) check (issue_amount >= 0),
total_edition INT NOT NULL DEFAULT (0) check (total_edition >= 0)
GO

--SELECT * FROM [author]
--GO

ALTER TABLE [book]
ADD title NVARCHAR(50) NOT NULL DEFAULT 'title',
edition INT NOT NULL DEFAULT (1) check (edition >=1),
published DATE,
issue INT NULL
GO

ALTER TABLE [book_author]
DROP FK_book_author_book
GO
ALTER TABLE [book]
DROP PK_book__ISBN
GO
UPDATE [book]
SET issue = 1
WHERE issue IS NULL
GO
ALTER TABLE [book]
Alter COLUMN issue INT NOT NULL 
GO
ALTER TABLE [book]
ADD CONSTRAINT def_issue DEFAULT 1 FOR issue
GO
ALTER TABLE [book]
ADD CONSTRAINT book_pk PRIMARY KEY (ISBN, issue)
GO
ALTER TABLE [book_author]
ADD issue INT NOT NULL DEFAULT 1
GO

ALTER TABLE [book_author]
ADD  CONSTRAINT FK_book_author_book FOREIGN KEY(ISBN, issue)
REFERENCES [book] (ISBN, issue)
ON UPDATE CASCADE
GO
--SELECT * FROM [book]
--GO
--SELECT * FROM [book_author]
--GO

ALTER TABLE [publisher]
ADD created DATE NOT NULL DEFAULT '19000101',
country NVARCHAR(50) NOT NULL DEFAULT 'USA',
city NVARCHAR(50) NOT NULL DEFAULT 'NY',
book_amount INT NOT NULL DEFAULT (0) check (book_amount >= 0),
issue_amount INT NOT NULL DEFAULT (0) check (issue_amount >= 0),
total_edition INT NOT NULL DEFAULT (0) check (total_edition >= 0)
GO

--SELECT * FROM [publisher]
--GO

ALTER TABLE [author_log]
ADD 
book_amount_old INT,
issue_amount_old INT,
total_edition_old INT,
book_amount_new INT,
issue_amount_new INT,
total_edition_new INT
GO

--SELECT * FROM [author_log]
--GO