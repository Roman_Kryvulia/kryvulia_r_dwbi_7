﻿USE [R_Kryvulia_Library]
GO
CREATE VIEW [vw_author]
AS
SELECT [name], [book_amount], [issue_amount], [total_edition] FROM [author]
WHERE [author_id]=128
GO

--check trigger [trg_for_book_author]

--check trigger on insert
SELECT * FROM [vw_author]
GO

--[book_amount]-38, [issue_amount]-86, [total_edition]-420144

--insert new book to 'sabatini_rafael'(author_id=128) with issue=10000
 INSERT INTO [book] (ISBN, publisher_id, URL, price, title, edition, published)  VALUES  
('5-7707-2062-1', (SELECT [publisher_id] FROM [publisher] WHERE [name] = 'Майдан'),
 'www.captain_blad_X.com.ua', 85,'Хроніки капітана Блада', 10000, '1993-01-01')
GO
INSERT INTO [book_author] (book_author_id, ISBN, author_id, seq_no, issue)  VALUES
(199, '5-7707-2062-1', (SELECT [author_id] FROM [author] WHERE [name] = 'sabatini_rafael'), 2,
(SELECT [issue] FROM [book] WHERE [URL] = 'www.captain_blad_X.com.ua'))
GO
--[book_amount]-38+1=39, [issue_amount]-86+1=87, [total_edition]-420144+10000=430144
SELECT * FROM [vw_author]
GO

--check trigger on delete
DELETE [book_author] 
WHERE ISBN = ('5-7707-2062-1')
--[book_amount]-39-1=38, [issue_amount]-87-1=86, [total_edition]-430144-10000=420144
SELECT * FROM [vw_author]
GO