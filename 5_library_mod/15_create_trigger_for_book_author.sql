﻿USE [R_Kryvulia_Library]
GO
DROP TRIGGER IF EXISTS [trg_book_author_to_author]
GO

CREATE TRIGGER [trg_book_author_to_author]
ON [book_author]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
SET @operation = 
CASE 
	WHEN @ins > 0 and @del > 0 THEN 'U'  
	WHEN @ins = 0 and @del > 0 THEN 'D'  
	WHEN @ins > 0 and @del = 0 THEN 'I'  
END

IF @operation = 'I'
BEGIN

UPDATE [author]
SET 
 [author].[book_amount] = [author].[book_amount] + [add_table_ins].[кількість книг],
 [author].[issue_amount] = [author].[issue_amount] + [add_table_ins].[кількість видань],
 [author].[total_edition] = [author].[total_edition] + [add_table_ins].[тираж]
 FROM [author]
 INNER JOIN  
 (SELECT  DISTINCT author_id, COUNT (author_id) [кількість книг], COUNT([book].issue) [кількість видань], SUM (edition) [тираж]
 FROM [inserted] INNER JOIN [book]
  ON  [inserted].[ISBN] = [book].[ISBN]
 GROUP BY [author_id])
 AS [add_table_ins] 
 ON [author].[author_id]= [add_table_ins].[author_id] 
END

IF @operation = 'D'
BEGIN

UPDATE [author]
SET 
 [author].[book_amount] = [author].[book_amount] - [add_table_del].[кількість книг],
 [author].[issue_amount] = [author].[issue_amount] - [add_table_del].[кількість видань],
 [author].[total_edition] = [author].[total_edition] - [add_table_del].[тираж]
 FROM [author]
 INNER JOIN
 (SELECT  DISTINCT author_id, COUNT (author_id) [кількість книг], COUNT([book].issue) [кількість видань], SUM (edition) [тираж]
 FROM [deleted] INNER JOIN [book]
  ON  [deleted].[ISBN] = [book].[ISBN]
 GROUP BY [author_id])
 AS [add_table_del] 
 ON [author].[author_id]= [add_table_del].[author_id] 
END

IF @operation = 'U'
BEGIN

UPDATE [author]
SET 
 [author].[book_amount] = [author].[book_amount] + [add_table_ins].[кількість книг] - [add_table_del].[кількість книг],
 [author].[issue_amount] = [author].[issue_amount] + [add_table_ins].[кількість видань] - [add_table_del].[кількість видань],
 [author].[total_edition] = [author].[total_edition] + [add_table_ins].[тираж] - [add_table_del].[тираж]
 FROM [author]
 INNER JOIN (SELECT  DISTINCT author_id, COUNT (author_id) [кількість книг], COUNT([book].issue) [кількість видань], SUM (edition) [тираж]
 FROM [inserted] INNER JOIN [book]
 ON  [inserted].[ISBN] = [book].[ISBN]
 GROUP BY [author_id])
 AS [add_table_ins] 
 ON [author].[author_id]= [add_table_ins].[author_id] 
INNER JOIN
 (SELECT  DISTINCT author_id, COUNT (author_id) [кількість книг], COUNT([book].issue) [кількість видань], SUM (edition) [тираж]
 FROM [deleted] INNER JOIN [book]
  ON  [deleted].[ISBN] = [book].[ISBN]
 GROUP BY [author_id])
 AS [add_table_del] 
 ON [author].[author_id]= [add_table_del].[author_id] 
END
END


