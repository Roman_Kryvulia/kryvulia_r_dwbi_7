﻿USE [labor_sql]
GO

--#51
SELECT [model], [cd],  COUNT(DISTINCT model) AS 'кількість моделей', COUNT(DISTINCT [cd]) AS 'кількість cd'
FROM [pc]
GROUP BY GROUPING SETS ([model],[cd])
GO
