﻿USE [labor_sql]
GO

--#49 (correct_script)
SELECT [town] [місто], SUM(all_tr) [загальна кількість рейсів]
FROM 
(SELECT [town_from] [town], COUNT(*)  [all_tr]
 FROM [trip]
 GROUP BY [town_from]
 UNION ALL
 SELECT [town_to], COUNT(*)
 FROM [trip]
 GROUP BY [town_to]) AS T
GROUP BY [town]
GO
