﻿USE [labor_sql]
GO
--#1
SELECT DISTINCT maker, type FROM [product]
WHERE type = 'laptop'
ORDER BY maker ASC
GO
--#2
SELECT model, ram, screen, price FROM [laptop]
WHERE price > 1000
ORDER BY ram ASC, price DESC
GO
--#3
SELECT * FROM [printer]
WHERE color = 'y'
ORDER BY price DESC
GO
--#4
SELECT model, speed, hd, cd, price FROM [pc]
WHERE price < 600 AND cd IN ('12x','24x') 
ORDER BY speed DESC
GO
--#5
SELECT name, class FROM [ships]
WHERE name = class
ORDER BY name ASC
GO
--#6
SELECT * FROM [pc]
WHERE speed !<500 AND price <800
ORDER BY price DESC
GO
--#7
SELECT * FROM [printer]
WHERE type !='matrix' AND price <300
ORDER BY type DESC
GO
--#8
SELECT model, speed FROM [pc]
WHERE price BETWEEN 400 AND 600 
ORDER BY hd ASC
GO
--#9
SELECT model, speed, hd, price FROM [laptop]
WHERE screen !< 12 
ORDER BY price DESC
GO
--#10
SELECT model, type, price FROM [printer]
WHERE price < 300 
ORDER BY type DESC
GO
--#11
SELECT model, ram, price FROM [laptop]
WHERE ram = 64 
ORDER BY screen ASC
GO
--#12
SELECT model, ram, price FROM [pc]
WHERE ram > 64 
ORDER BY hd ASC
GO
--#13
SELECT model, speed, price FROM [pc]
WHERE speed BETWEEN 500 AND 750 
ORDER BY hd DESC
GO
--#14
SELECT * FROM [outcome_o]
WHERE out > 2000 
ORDER BY date DESC
GO
--#15
SELECT * FROM [income_o]
WHERE inc BETWEEN 5000 AND 10000 
ORDER BY inc ASC
GO
--#16
SELECT * FROM [income]
WHERE point  = 1 
ORDER BY inc ASC
GO
--#17
SELECT * FROM [outcome]
WHERE point  = 2 
ORDER BY out ASC
GO
--#18
SELECT * FROM [classes]
WHERE country = 'japan' 
ORDER BY type DESC
GO
--#19
SELECT name, launched FROM [ships]
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC
GO
--#20
SELECT ship, battle, result FROM [outcomes]
WHERE battle = 'Guadalcanal' AND result != 'sunk'
ORDER BY ship DESC
GO
--#21
SELECT ship, battle, result FROM [outcomes]
WHERE result = 'sunk'
ORDER BY ship DESC
GO
--#22
SELECT class, displacement FROM [classes]
WHERE displacement !<40000
ORDER BY type ASC
GO
--#23
SELECT trip_no, town_from, town_to FROM [trip]
WHERE town_from = 'london' OR town_to = 'london'
ORDER BY time_out ASC
GO
--#24
SELECT trip_no, plane, town_from, town_to FROM [trip]
WHERE plane = 'TU-134'
ORDER BY time_out DESC
GO
--#25
SELECT trip_no, plane, town_from, town_to FROM [trip]
WHERE plane != 'IL-86'
ORDER BY plane ASC
GO
--#26
SELECT trip_no, town_from, town_to FROM [trip]
WHERE NOT (town_from = 'Rostov' OR town_to = 'Rostov')
ORDER BY plane ASC
GO
--#27
SELECT DISTINCT model FROM [product]
WHERE type = 'PC' AND model LIKE '%1%1%'
GO
--#28
SELECT * FROM [outcome]
WHERE CONVERT(VARCHAR(50), date , 120) LIKE  '____-03-%' 
GO
--#29
SELECT * FROM [outcome_o]
WHERE CONVERT(VARCHAR(50), date , 120) LIKE  '____-__-14%' 
GO
--#30
SELECT name FROM [ships]
WHERE name LIKE 'w%n'
GO
--#31
SELECT name FROM [ships]
WHERE name LIKE '%e%e%'
GO
--#32
SELECT name, launched FROM [ships]
WHERE name LIKE '%[^a]'
GO
--#33
SELECT name FROM [battles]
WHERE name LIKE '%[ ]%[^C]'
GO
--#34
SELECT * FROM [trip]
WHERE time_out BETWEEN '1900-01-01 12:00:00.000' AND '1900-01-01 17:00:00.000'
GO
--#35
SELECT * FROM [trip]
WHERE time_in BETWEEN '1900-01-01 17:00:00.000' AND '1900-01-01 23:00:00.000'
GO
--#36
SELECT * FROM [trip]
WHERE time_in BETWEEN '1900-01-01 21:00:00.000' AND '1900-01-01 23:59:59.000'
OR time_in BETWEEN '1900-01-01 00:00:00.000' AND '1900-01-01 10:00:00.000'
GO
--#37
SELECT date FROM [pass_in_trip]
WHERE place LIKE '1%'
GO
--#38
SELECT date FROM [pass_in_trip]
WHERE place LIKE '%C'
GO
--#39
SELECT SUBSTRING (name,(CHARINDEX(' ', name)) + 1, LEN(name)) AS прізвище
FROM [passenger]
WHERE name LIKE '%[ ]C%'
GO
--#40 
--variant-1
SELECT SUBSTRING (name,(CHARINDEX(' ', name)) + 1, LEN(name)) AS прізвище
FROM [passenger]
WHERE name NOT LIKE '%[ ][J]%'
GO
--variant-2
SELECT REVERSE(LEFT(REVERSE(RTRIM(name)), CHARINDEX(' ', REVERSE(RTRIM(name)) + ' ', 1) - 1))
FROM [passenger]
WHERE name NOT LIKE '%[ ][J]%'
GO
--#41
SELECT
CONCAT(N'середня ціна=', AVG (price)) AS [середня ціна=]
FROM [laptop]
GO
--#42
SELECT
CONCAT(N'код:', code) AS code,
CONCAT(N'модель:', model) AS model,
CONCAT(N'швидкість:', speed) AS speed,
CONCAT(N'обєм памяті:', ram) AS ram,
CONCAT(N'розмір диску:', hd) AS hd,
CONCAT(N'швидкість CD-приводу:', cd) AS cd,
CONCAT(N'ціна:', price) AS price
FROM  [pc]
GO
--#43
SELECT CONVERT(varchar, date, 102) AS date
FROM  [income]
GO
--#44
SELECT ship, battle,
CASE result
WHEN 'sunk' THEN N'потоплений'
WHEN 'damaged' THEN N'пошкоджений'
ELSE N'цілий'
END AS результат
FROM [outcomes] 
GO
--#45
SELECT
CONCAT(N'ряд:', SUBSTRING (place, 1, 1)) AS ряд,
CONCAT (N'місце:', SUBSTRING (place, 2, 1)) AS місце
FROM [pass_in_trip]
GO
--#46
SELECT
CONCAT('from  ', RTRIM(town_from),'  to  ', town_to)
FROM [trip]
GO
--#47
SELECT 
CONCAT(
LEFT (trip_no,1), RIGHT (RTRIM(trip_no),1),
LEFT (ID_comp,1), RIGHT (ID_comp,1),
LEFT (plane,1), RIGHT (RTRIM(plane),1),
LEFT (town_from,1), RIGHT (RTRIM(town_from),1),
LEFT (town_to,1), RIGHT (RTRIM(town_to),1),
LEFT (time_out,1), RIGHT (RTRIM(time_out),1),
LEFT (time_in,1), RIGHT (RTRIM(time_in),1) 
) [об'єднане значення]
FROM [trip]
GO

--#48
SELECT DISTINCT maker, count (DISTINCT model) AS 'число моделей'
FROM [product]
GROUP BY maker, type
HAVING count(DISTINCT model) > 1 AND type = 'pc'
GO
--#49
--variant_1
SELECT town_to [місто],
count (town_to) [кількість прильотів],
count (town_from) [кількість відльотів],
count (town_to)+count (town_from) [загальна кількість]
FROM [trip]
GROUP BY town_to
GO
--variant_2
SELECT t[місто], count (*) [кількість] FROM
((SELECT town_to AS t FROM [trip])
UNION ALL
(SELECT town_to AS t FROM [trip]))
AS z
GROUP BY t
--#50
SELECT DISTINCT type, count(DISTINCT model) AS 'кількість моделей'
FROM [printer]
GROUP BY type
GO 
--#51
SELECT [model], [cd],  COUNT(DISTINCT model) AS 'кількість моделей', COUNT(DISTINCT [cd]) AS 'кількість cd'
FROM [pc]
GROUP BY GROUPING SETS ([model],[cd])
GO
--#52
SELECT trip_no [рейс],
convert(varchar,time_in-time_out,8) [тривалість польоту]
FROM [trip]
--#53
SELECT 
	CASE 
		WHEN point IS NULL THEN N'усі пункти' 
		ELSE CAST (point AS VARCHAR(20)) 
	END AS [пункт],
	CASE 
		WHEN date IS NULL THEN N'усі дати' 
		ELSE CAST (date AS VARCHAR(20)) 
	END AS [дата],
SUM (out) [сума],  MIN (out) [мін],  MAX (out) [макс]
FROM[outcome]
GROUP BY ROLLUP (point, date)
GO
--#54
SELECT trip_no [рейс], SUBSTRING (place, 1, 1) AS [ряд],
COUNT(SUBSTRING (place, 2, 1)) AS [кількість зайнятих місць]
FROM [pass_in_trip]
GROUP BY trip_no, SUBSTRING (place, 1, 1)
ORDER BY trip_no
GO
--#55
--varaint-1
SELECT COUNT (SUBSTRING (name,(CHARINDEX(' ', name)) + 1, LEN(name))) [кількість прізвищ S,B,A]
FROM [passenger]
WHERE name LIKE '%[ ]S%' OR name LIKE '%[ ]B%'
OR name LIKE '%[ ]A%'
GO
--variant-2
SELECT
CONCAT(N'пасажири на S=', COUNT (SUBSTRING (name,(CHARINDEX(' ', name)) + 1, LEN(name)))) [кількість прізвищ]
FROM [passenger]
WHERE name LIKE '%[ ]S%'
UNION ALL
SELECT
CONCAT(N'пасажири на B=', COUNT (SUBSTRING (name,(CHARINDEX(' ', name)) + 1, LEN(name)))) [кількість прізвищ]
FROM [passenger]
WHERE name LIKE '%[ ]B%'
UNION ALL
SELECT
CONCAT(N'пасажири на A=', COUNT (SUBSTRING (name,(CHARINDEX(' ', name)) + 1, LEN(name)))) [кількість прізвищ]
FROM [passenger]
WHERE name LIKE '%[ ]A%'
GO

