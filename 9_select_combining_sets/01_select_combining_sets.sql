USE [labor_sql]
GO

--#1
;WITH [print_cte] AS
(SELECT p.[maker], pr.[type], pr.[price] FROM [product] p JOIN [printer] pr
ON p.[model] = pr.[model]
WHERE pr.[type] != 'laser')

SELECT [maker], [type], [price] FROM [print_cte]
GO

--#2
;WITH [print_cte] AS
(SELECT p.[maker], pr.[type], pr.[price] FROM [product] p JOIN [printer] pr
ON p.[model] = pr.[model]
WHERE pr.[type] != 'laser'),
[avg_price_cte] AS 
(SELECT [maker], [type], [price] FROM [print_cte]
WHERE [price] >(SELECT AVG(price) FROM [print_cte]))

SELECT [maker], [type], [price] FROM [avg_price_cte]
GO

--#3
;WITH [cteR] ([region_id], [place_id], [name], [place_level]) AS
  (    SELECT [region_id], [id], [name], 0 FROM [geography]
    WHERE [region_id] IS NULL
    UNION ALL
    SELECT g.[region_id], g.[id], g.[name], r.[place_level]+1
    FROM [geography] g JOIN [cteR] r
        ON g.[region_id] = r.[place_id])

SELECT [region_id], [place_id], [name],
 (SELECT [id] FROM [geography]
    WHERE [id] = [cteR].[place_level]) AS [place_level] 
FROM [cteR]
WHERE [region_id] = 1
ORDER BY [region_id]
GO

--#4
;WITH [cteR] ([region_id], [place_id], [name], [place_level]) AS
(SELECT [region_id], [id], [name], 0 FROM [geography]
 WHERE [region_id] = 4
 UNION ALL
 SELECT g.[region_id], g.[id], g.[name], r.[place_level]+1
 FROM [geography] g JOIN [cteR] r  ON g.[region_id] = r.[place_id])

SELECT * FROM [cteR]
GO

--#5
; WITH [row_CTE] (row_num) AS  
(SELECT [row_num] = 1    
UNION ALL
SELECT  [row_num] + 1 FROM [row_CTE] WHERE [row_num] < 10000)  
 
SELECT * FROM [row_CTE]
OPTION (maxrecursion 0)
GO

--#6
;WITH [row_CTE] (row_num) AS  
(SELECT [row_num] = 1    
UNION ALL
SELECT  [row_num] + 1 FROM [row_CTE] WHERE [row_num] < 100000)  
 
SELECT * FROM [row_CTE]
OPTION (maxrecursion 0)
GO

--#7
DECLARE
 @start_date DATE = '2018-01-01',
 @end_date DATE= '2018-12-31';
 
;WITH [calendar_cte] AS (
SELECT @start_date AS [date]
UNION ALL
SELECT DATEADD(DAY , 1, [date]) AS [date]
FROM [calendar_cte]
WHERE DATEADD (DAY, 1, [date]) <= @end_date)

SELECT COUNT(DATENAME(weekday,[date])) AS [days_of_weekend]
FROM [calendar_cte]
WHERE DATENAME(weekday,[date]) IN ('Saturday', 'Sunday')
OPTION (MAXRECURSION 0)
GO

--#8
SELECT DISTINCT [maker] FROM [product]
WHERE [maker] NOT IN (SELECT [maker] FROM [product] WHERE [type] IN ('laptop'))
  AND [maker] IN (SELECT [maker] FROM [product] WHERE [type] IN ('PC'))
GO

--#9
SELECT DISTINCT [maker] FROM [product]
WHERE [maker] <> ALL (SELECT [maker] FROM [product] WHERE [type] IN ('laptop'))
  AND [maker] IN (SELECT [maker] FROM [product] WHERE [type] IN ('PC'))
GO

--#10
SELECT DISTINCT [maker] FROM [product]
WHERE [maker] <> ALL (SELECT [maker] FROM [product] WHERE [type] IN ('laptop'))
  AND [maker] = ANY (SELECT [maker] FROM [product] WHERE [type] IN ('PC'))
GO

--#11
SELECT DISTINCT [maker] FROM [product]
WHERE [maker] IN (SELECT [maker] FROM [product] WHERE [type] IN ('laptop'))
  AND [maker] IN (SELECT [maker] FROM [product] WHERE [type] IN ('PC'))
GO

--#12
SELECT DISTINCT [maker] FROM [product]
WHERE [maker] IN (SELECT [maker] FROM [product] WHERE [type] = ALL (SELECT [type] FROM [product] WHERE [type] = 'laptop'))
  AND [maker] IN (SELECT [maker] FROM [product] WHERE [type] = ALL (SELECT [type] FROM [product] WHERE [type] ='PC'))
GO

--#13
SELECT DISTINCT [maker] FROM [product]
WHERE [maker] =ANY (SELECT [maker] FROM [product] WHERE [type] IN ('laptop'))
  AND [maker] =ANY (SELECT [maker] FROM [product] WHERE [type] IN ('PC'))
GO

--#14
SELECT DISTINCT [maker] FROM [product] 
WHERE [maker] = ANY (SELECT DISTINCT [maker] FROM [product] 
                WHERE [model] IN (SELECT [model] FROM [pc]))
AND [maker] != ALL (SELECT DISTINCT [maker] FROM [product] 
WHERE [model] NOT IN (SELECT [model] FROM [pc]) AND [type]= 'pc')
GO

--#15
SELECT [country],[class]
FROM [classes]
WHERE [country] = 'Ukraine'
UNION
SELECT [country],[class]
FROM [classes]
WHERE [country] = ALL (SELECT [country] FROM [classes] WHERE [country] = 'Ukraine')
GO

--#16
SELECT o.[ship], o.[battle], b.[date] FROM [outcomes] o 
JOIN [battles] b ON o.[battle] = b.[name]
WHERE o.[ship] IN (SELECT [ship] FROM [outcomes]
                  GROUP BY [ship]
                  HAVING COUNT(battle) >1)
AND
o.[ship] IN (SELECT [ship] FROM [outcomes]
             WHERE [result] = 'damaged')
GO

--#17
 SELECT DISTINCT [maker] FROM [product] 
 WHERE [type] = 'pc'
 AND [maker] NOT IN 
 (SELECT DISTINCT [maker] FROM [product] p 
 WHERE p.[type]= 'pc' AND NOT EXISTS (SELECT * FROM [pc] WHERE pc.[model] = p.[model]))
 GO
 
--#18
SELECT DISTINCT [maker] FROM [product]
WHERE [maker] IN (SELECT [maker] FROM [product] WHERE [type] IN ('printer'))
  AND [maker] IN (SELECT [maker] FROM [product] WHERE [type] IN ('PC'))
  AND [maker] IN (SELECT [maker] FROM [product] 
                  WHERE [model] IN (SELECT [model] FROM [PC] WHERE [speed] IN (SELECT TOP(2) max(speed) FROM [pc])))
GO

--#19
SELECT [class] FROM [ships]
WHERE [name] IN (SELECT [ship] FROM [outcomes] WHERE [result] = 'sunk')
UNION
SELECT c.[class] FROM [outcomes] o JOIN [classes] c
ON o.[ship] = c.[class]
WHERE [result] = 'sunk'
GO

--#20
SELECT [model], [price] FROM [printer]
WHERE [price] = (SELECT MAX(price) FROM [printer])
GO

--#21
SELECT p.[type], l.[model], l.[speed] FROM [laptop] l 
JOIN [product] p ON p.[model] = l.[model]
WHERE l.[speed] < (SELECT MIN(speed) FROM [pc])
GO

--#22
SELECT p.[maker], pr.[price]
FROM [printer] pr JOIN [product] p
ON p.[model] = pr.[model]
WHERE [price] = (SELECT MIN(price) FROM [printer] WHERE [color] = 'y')
  AND [color] = 'y'
GO

--#23
SELECT DISTINCT o.[battle],  p.[country], COUNT(p.[ship]) [quantity_of_ships]
FROM [outcomes] o 
JOIN
(SELECT  s.[name] [ship], c.[country] [country]
FROM [ships] s JOIN [classes] c ON s.[class] = c.[class]
WHERE [name] IN (SELECT [ship] FROM [outcomes]) ) AS [p]
ON o.[ship] = p.[ship]
GROUP BY o.[battle],  p.[country]
HAVING COUNT(p.[ship])>1
GO 

--#24
SELECT pro.[maker], 
(SELECT COUNT(*) FROM [pc] JOIN [product] p ON pc.[model] = p.[model] WHERE p.[maker]= pro.[maker]) AS [pc],
(SELECT COUNT(*) FROM [laptop] l JOIN [product] p ON l.[model] = p.[model] WHERE p.[maker]= pro.[maker]) AS [laptop],
(SELECT COUNT(*) FROM [printer] pr  JOIN [product] p ON pr.[model] = p.[model] WHERE p.[maker]= pro.[maker]) AS [printer]
FROM [product] [pro]
GROUP BY pro.[maker]
GO

--#25
SELECT [maker], 
(SELECT CASE WHEN  COUNT(pc.[model]) = 0 THEN 'no' ELSE CONCAT('yes (', COUNT(pc.[model]),')') END) [pc]
FROM [product] p LEFT JOIN [pc] ON p.[model] = pc.[model]
GROUP BY [maker]
GO

--#26
SELECT o.[point], o.[date], i.[inc], o.[out] 
FROM [outcome_o] o LEFT JOIN [income_o] i ON o.[date]= i.[date] AND o.[point]=i.[point]
UNION
SELECT i.[point], i.[date], i.[inc], o.[out] 
FROM [outcome_o] o RIGHT JOIN [income_o] i ON o.[date]= i.[date] AND o.[point]=i.[point]
GO

--#27
SELECT b.[name], b.[numGuns], b.[bore], b.[displacement], b.[type],b.[country], b.[launched], b.[class]
FROM 
(SELECT s.[name], c.[numGuns], c.[bore], c.[displacement], c.[type], c.[country], s.[launched], s.[class],
CASE WHEN c.[numGuns] = 8 THEN 1 ELSE 0 END AS [k1],
CASE WHEN c.[bore] = 15 THEN 1 ELSE 0 END AS [k2],
CASE WHEN c.[displacement] = 32000 THEN 1 ELSE 0 END AS [k3],
CASE WHEN c.[type] = 'bb' THEN 1 ELSE 0 END AS [k4],
CASE WHEN c.[country] = 'USA' THEN 1 ELSE 0 END AS [k5],
CASE WHEN s.[launched] = 1915 THEN 1 ELSE 0 END AS [k6],
CASE WHEN s.[class] = 'Kon' THEN 1 ELSE 0 END AS [k7]
FROM [ships] [s] JOIN  [classes] c ON s.[class]= c.[class]) [b]
WHERE (b.[k1] + b.[k2] + b.[k3] + b.[k4] + b.[k5] + b.[k6] + b.[k7]) >= 4
GO

--#28
SELECT os.[point], os.[date],
CASE WHEN os.[out] > p.[sum_out] THEN 'once a day' 
     WHEN os.[out] = p.[sum_out] THEN 'both'
	 ELSE 'more than once a day' END AS [result] 
FROM (SELECT o.[point], o.[date], SUM(o.[out]) AS [sum_out]
FROM [outcome] o
GROUP BY [date], [point]) [p]
FULL JOIN [outcome_o] [os] ON p.[date]= os.[date] AND p.[point] = os.[point]
GO

--#29
SELECT DISTINCT p.[maker], pc.[model], p.[type], pc.[price]
FROM [product] p JOIN [pc] ON p.[model] = pc.[model]
WHERE [maker] = 'B'
UNION
SELECT DISTINCT p.[maker], l.[model], p.[type], l.[price]
FROM [product] p JOIN [laptop] l ON p.[model] = l.[model]
WHERE [maker] = 'B'
UNION
SELECT DISTINCT p.[maker], pr.[model], p.[type], pr.[price]
FROM [product] p JOIN [printer] pr ON p.[model] = pr.[model]
WHERE [maker] = 'B'
GO
--#30
SELECT [name], [class] FROM [ships] 
WHERE [name] = [class]
UNION
SELECT DISTINCT o.[ship], c.[class]
FROM [outcomes] o JOIN [classes] c ON o.[ship] = c.[class]
WHERE [ship]  NOT IN (SELECT [name] FROM [ships]) 
GO

--#31
SELECT [class]--, COUNT(class) [count_ships]
FROM [ships]
GROUP BY [class]
HAVING COUNT([class])<2 
UNION
SELECT DISTINCT [ship]--, COUNT([ship]) [count_ships]
FROM [outcomes] 
WHERE [ship]  NOT IN (SELECT [name] FROM [ships]) AND
      [ship] IN (SELECT [class] FROM [classes])
GROUP BY [ship]
HAVING COUNT([ship])<2 
GO

--#32
SELECT [name]--, [launched]
FROM [ships]
WHERE [launched] < '1942'
UNION
SELECT DISTINCT o.[ship]--, (SELECT YEAR(b.[date])) [launched]
FROM [outcomes] o JOIN [battles] b ON o.[battle] = b.[name]
WHERE o.[ship]  NOT IN (SELECT [name] FROM [ships]) AND
      b.[date] < '1942'
GO