USE [labor_sql]
GO

--#1
SELECT
ROW_NUMBER() OVER(ORDER BY [id_comp], [trip_no]) AS [num],
[trip_no],
[id_comp]
FROM [trip]
GO

--#2
SELECT
ROW_NUMBER() OVER(PARTITION BY [id_comp] ORDER BY [trip_no]) AS [num],
[trip_no],
[id_comp]
FROM [trip]
GO

--#3
    --variant-1
SELECT [model], [color], [type], [price]
FROM 
(SELECT *, RANK() OVER(PARTITION BY [type] ORDER BY [price]) [rk]  FROM [printer]) [T]
WHERE [rk] = 1
GO
   --variant-2
SELECT [model],  [color], [type], [price]
FROM [printer]
WHERE [type] = 'jet'
AND [price] = (SELECT MIN(price) FROM [printer] WHERE [type] = 'jet')
UNION 
SELECT [model],  [color], [type], [price]
FROM [printer]
WHERE [type] = 'laser'
AND [price] = (SELECT MIN(price) FROM [printer] WHERE [type] = 'laser')
UNION 
SELECT [model],  [color], [type], [price]
FROM [printer]
WHERE [type] = 'matrix'
AND [price] = (SELECT MIN(price) FROM [printer] WHERE [type] = 'matrix')
ORDER BY [type]
GO

--#4
SELECT [maker] 
FROM 
(SELECT [maker], RANK() OVER(PARTITION BY [maker] ORDER BY [model]) [rk]
FROM [product] 
WHERE [type] = 'PC') [T]
WHERE [rk] = 3
GO
 
--#5
SELECT [price]
FROM (SELECT DENSE_RANK () OVER (ORDER BY [price] DESC) AS [den_rank], [price] FROM [pc]) [T]
WHERE [den_rank] = 2
GO

--#6
SELECT *, 
NTILE(3) OVER (ORDER BY(REVERSE(LEFT(REVERSE(RTRIM([name])), CHARINDEX(' ', REVERSE(RTRIM([name])) + ' ', 1) - 1)))) AS [group]
FROM [passenger]
GO

--#7
SELECT *,
ROW_NUMBER() OVER(ORDER BY [price] DESC) AS [id],
(SELECT TOP(1)ROW_NUMBER() OVER(ORDER BY [price]) [id] FROM [pc]
ORDER BY [id] DESC )[row_total],
NTILE(4) OVER(ORDER BY [price] DESC) [page_num],
(SELECT TOP(1)NTILE(4) OVER(ORDER BY [price] DESC) [page_num]  FROM [pc]
ORDER BY [page_num] DESC) [page_total]
FROM [pc]
GO

--#8
SELECT TOP(1)* FROM
(
SELECT TOP(1) 
MAX([inc]) OVER(ORDER BY ([inc]) DESC) [max_sum], (SELECT 'inc') [type],
[date],[point]
FROM [income]
UNION
SELECT TOP(1) 
MAX([inc]) OVER(ORDER BY ([inc]) DESC) [max_sum],  (SELECT 'inc') [type],
[date],[point]
FROM [income_o]
UNION
SELECT TOP(1) 
MAX([out]) OVER(ORDER BY ([out]) DESC) [max_sum], (SELECT 'out') [type],
[date],[point]
FROM [outcome]
UNION
SELECT TOP(1)
MAX([out]) OVER(ORDER BY ([out]) DESC) [max_sum], (SELECT 'out') [type],
[date],[point]
FROM [outcome_o]) T
ORDER BY ([max_sum]) DESC
GO

--#9
SELECT *,
([price] -AVG(price) OVER(PARTITION BY ([speed]))) [dif_local_price],
([price] - (SELECT AVG ([price]) FROM [pc])) [dif_total_price],
(SELECT AVG ([price]) FROM [pc]) [total_price]
FROM [pc]
ORDER BY [speed]
GO

