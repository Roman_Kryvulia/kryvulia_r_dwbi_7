USE [master]
DROP DATABASE IF EXISTS [R_Kryvulia_Library_synonym]
GO
CREATE DATABASE [R_Kryvulia_Library_synonym]
GO

USE [R_Kryvulia_Library_synonym]
GO


CREATE SYNONYM [sn_author]
FOR R_Kryvulia_Library.dbo.author  
GO 

CREATE SYNONYM [sn_publisher]
FOR R_Kryvulia_Library.dbo.publisher  
GO

CREATE SYNONYM [sn_book]
FOR R_Kryvulia_Library.dbo.book  
GO 

CREATE SYNONYM [sn_book_author]
FOR R_Kryvulia_Library.dbo.book_author  
GO

CREATE SYNONYM [sn_author_log]
FOR R_Kryvulia_Library.dbo.author_log 
GO


SELECT * FROM [sn_author]
SELECT * FROM [sn_publisher]
SELECT * FROM [sn_book]
SELECT * FROM [sn_book_author]
SELECT * FROM [sn_author_log]

