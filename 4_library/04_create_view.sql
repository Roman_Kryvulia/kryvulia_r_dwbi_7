-- create database
USE [master]
DROP DATABASE IF EXISTS [R_Kryvulia_Library_view]
GO
CREATE DATABASE [R_Kryvulia_Library_view]
GO

-- create view
USE [R_Kryvulia_Library_view]
GO

 CREATE VIEW [vw_author] (author_id,name, URL, insterted, insterted_by, updated, updated_by) 
 AS
 SELECT * FROM [R_Kryvulia_Library].[dbo].[author]
 GO

 CREATE VIEW [vw_publisher] ( publisher_id, name, URL, insterted, insterted_by, updated, updated_by) 
 AS
 SELECT * FROM [R_Kryvulia_Library].[dbo].[publisher]
 GO

 CREATE VIEW [vw_book] ( ISBN, publisher_id, URL, price, insterted, insterted_by, updated, updated_by) 
 AS
 SELECT * FROM [R_Kryvulia_Library].[dbo].[book]
 GO

 CREATE VIEW [vw_book_author] (book_author_id, ISBN, author_id, seq_no, insterted, insterted_by, updated, updated_by) 
 AS
 SELECT * FROM [R_Kryvulia_Library].[dbo].[book_author]
 GO

 CREATE VIEW [vw_author_log] (operation_id, author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type, operation_datetime) 
 AS
 SELECT * FROM [R_Kryvulia_Library].[dbo].[author_log]
 GO


 SELECT * FROM [vw_author]
 GO
 SELECT * FROM [vw_publisher]
 GO
 SELECT * FROM [vw_book]
 GO
 SELECT * FROM [vw_book_author]
 GO
 SELECT * FROM [vw_author_log]
 GO
