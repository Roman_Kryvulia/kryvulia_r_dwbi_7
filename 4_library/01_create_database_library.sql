USE [master]
GO
DROP DATABASE IF EXISTS [R_Kryvulia_Library]
GO
CREATE DATABASE [R_Kryvulia_Library]
GO

ALTER DATABASE [R_Kryvulia_Library] ADD FILEGROUP [Data] 

ALTER DATABASE [R_Kryvulia_Library] 
ADD FILE ( NAME = N'R_Kryvulia_Library_data', 
    FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\R_Kryvulia_Library_data.mdf' , 
    SIZE = 5120KB , FILEGROWTH = 1024KB ) TO FILEGROUP [Data]
GO

IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'Data') 
ALTER DATABASE [R_Kryvulia_Library] MODIFY FILEGROUP [Data] DEFAULT
GO

