
USE [R_Kryvulia_Library]
GO
DROP TRIGGER IF EXISTS [tr_publisherUPD] 
GO
DROP TRIGGER IF EXISTS [tr_bookUPD] 
GO
DROP TRIGGER IF EXISTS [tr_book_authorUPD] 
GO
DROP TRIGGER IF EXISTS [tr_authorIUD_ON_author_log]
GO

 --create trigger_update on table [publisher]
CREATE TRIGGER [tr_publisherUPD] ON [publisher]
AFTER UPDATE
AS
BEGIN
  UPDATE [publisher]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE publisher.publisher_id = inserted.publisher_id
 END
 GO

 --create trigger_update on table [book]
CREATE TRIGGER [tr_bookUPD] ON [book]
AFTER UPDATE
AS
BEGIN
  UPDATE [book]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE book.ISBN = inserted.ISBN
END
GO

--create trigger_update on table [book_author]
CREATE TRIGGER [tr_book_authorUPD] ON [book_author]
AFTER UPDATE
AS
BEGIN
  UPDATE [book_author]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE book_author.book_author_id = inserted.book_author_id
END
GO

--create trigger_ins,del,upd on table [author]
CREATE TRIGGER [tr_authorIUD_ON_author_log] ON [author]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation CHAR(1)
DECLARE @ins INT = (SELECT COUNT(*) FROM INSERTED)
DECLARE @del INT = (SELECT COUNT(*) FROM DELETED)
SET @operation = 
CASE 
	WHEN @ins > 0 AND @del > 0 THEN 'U'  
	WHEN @ins = 0 AND @del > 0 THEN 'D'  
	WHEN @ins > 0 AND @del = 0 THEN 'I'  
END 
---- insert
IF @operation = 'I'
BEGIN

	INSERT INTO [author_log]
            (author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type)
	SELECT 
		inserted.author_id, 
		inserted.name,
		inserted.URL,
		NULL,
		NULL,
		NULL,
		@operation 
		FROM [author]
	inner join inserted on [author].author_id = inserted.author_id
END

---- delete
IF @operation = 'D'
BEGIN
	INSERT INTO [author_log]
	(author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type)
	SELECT 
		NULL,
		NULL,
		NULL,
		deleted.author_id, 
		deleted.name,
		deleted.URL,
		@operation 
	FROM deleted 
END
--- update (+update table [author])
IF @operation = 'U'
BEGIN
	 UPDATE [author]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE author.author_id = inserted.author_id
 			insert into [author_log]
			(author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type)
			select
		    inserted.author_id, 
		    inserted.name,
		    inserted.URL,
			deleted.author_id, 
		    deleted.name,
		    deleted.URL,
		    @operation
			FROM [author]
				 inner join  inserted on [author].author_id = inserted.author_id
				 inner join deleted on [author].author_id = deleted.author_id
				 			 
	END

END
GO

--sp_settriggerorder @triggername = '[tr_authorIUD_ON_author_log]', @order = 'first', @stmttype = 'UPDATE'
--sp_settriggerorder @triggername = '[tr_authorUPD]', @order = 'last', @stmttype = 'UPDATE'


