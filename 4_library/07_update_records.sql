USE [R_Kryvulia_Library]
GO

UPDATE [author] 
SET URL ='www.author.org' 
WHERE author_id BETWEEN 20 AND 40
GO

UPDATE [publisher]
SET URL ='www.publisher.com' 
WHERE publisher_id <70
GO

UPDATE [book]
SET price = price*0.9
WHERE price>=130
GO

UPDATE [book_author]
SET seq_no = seq_no+1
WHERE author_id BETWEEN 14 AND 36
GO




SELECT * FROM [author]
SELECT * FROM [publisher]
SELECT * FROM [book]
SELECT * FROM [book_author]
SELECT * FROM [author_log]
GO
