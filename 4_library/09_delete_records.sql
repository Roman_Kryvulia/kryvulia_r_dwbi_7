USE [R_Kryvulia_Library]
GO


DELETE [book_author] 
WHERE author_id !> 18
GO

DELETE [author] 
WHERE author_id !> 18
GO

DELETE [book] 
WHERE ISBN IN (
'978-7-20-123456-1','978-7-20-123456-2','978-7-20-123456-3','978-7-20-123456-4','978-7-20-123456-5'
) 
GO


DELETE [publisher] 
WHERE publisher_id BETWEEN 80 AND 88
GO


--SELECT * FROM [book_author]
--SELECT * FROM [author]
--SELECT * FROM [book]
--SELECT * FROM [publisher]
--SELECT * FROM [author_log]