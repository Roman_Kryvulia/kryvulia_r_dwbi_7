USE [R_Kryvulia_Library]
GO

CREATE TRIGGER tr_no_delete_records ON [author_log]
INSTEAD OF DELETE
AS
BEGIN
       PRINT 'Error delete, you cannot delete author_log'
       ROLLBACK TRANSACTION
END
GO

--check
--unsuccessful delete column author_log
--DELETE [author_log] WHERE name_new = 'franko'
--GO

--SELECT * FROM [author_log] WHERE name_new = 'franko'
