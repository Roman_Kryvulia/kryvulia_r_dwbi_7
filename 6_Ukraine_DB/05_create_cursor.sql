USE [people_ua_db]
GO

DECLARE [crs_insert_people] CURSOR FOR 
SELECT [surname], [sex], [amount] FROM [surname_list_identity]
GO

OPEN [crs_insert_people] 

DECLARE @surname NVARCHAR(20)
DECLARE @sex CHAR(1)
DECLARE @sex_t CHAR(1)
DECLARE @amount INT
DECLARE @name NVARCHAR(20)
DECLARE @married CHAR(3)
DECLARE @id_area INT
DECLARE @id_language INT
DECLARE @date_of_birth DATE
DECLARE @DateStart DATE = '1925-01-01'
DECLARE	@DateEnd DATE = '1999-12-31'
DECLARE @c INT

FETCH NEXT FROM [crs_insert_people] INTO @surname, @sex, @amount
WHILE @@FETCH_STATUS = 0
BEGIN
IF @sex IS NULL
WHILE @amount > 0
BEGIN  
   SET @name = (SELECT TOP 1 [name] FROM [name_list_identity] ORDER BY NEWID())
   SET @sex_t = (SELECT [sex] FROM [name_list_identity] WHERE [name_list_identity].[name] = @name)
   SET @date_of_birth = (SELECT DATEADD (DAY, RAND() * DATEDIFF(DAY, @DateStart, @DateEnd), @DateStart))
   SET @id_area = (SELECT ROUND((((SELECT COUNT(*) FROM [area_of_residence])-1)*RAND()+1),0))
   SET @id_language = (SELECT ROUND((((SELECT COUNT(*) FROM [native_language])-1)*RAND()+1),0))
   SET @married = CASE WHEN (SELECT ROUND((1*RAND()+1),0))=1 THEN 'yes' ELSE 'no'
 END
   INSERT INTO [people] ([surname], [name], [sex], [date_of_birth], [married], [id_area], [id_language]) VALUES
	                        (@surname, @name, @sex_t, @date_of_birth, @married, @id_area, @id_language)
   SET @amount -=1
END
IF @sex = 'm'
WHILE @amount > 0
  BEGIN
   SET @name = (SELECT TOP 1 [name] FROM [name_list_identity]
	                                  WHERE [name_list_identity].[sex] = 'm'
                                      ORDER BY NEWID())
   SET @date_of_birth = (SELECT DATEADD (DAY, RAND() * DATEDIFF(DAY, @DateStart, @DateEnd), @DateStart))
   SET @id_area = (SELECT ROUND((((SELECT COUNT(*) FROM [area_of_residence])-1)*RAND()+1),0))
   SET @id_language = (SELECT ROUND((((SELECT COUNT(*) FROM [native_language])-1)*RAND()+1),0))
   SET @married = CASE WHEN (SELECT ROUND((1*RAND()+1),0))=1 THEN 'yes' ELSE 'no'
 END
   INSERT INTO [people] ([surname], [name], [sex], [date_of_birth], [married], [id_area], [id_language]) VALUES
	                        (@surname, @name, 'm', @date_of_birth, @married, @id_area, @id_language)
	 SET @amount -=1
    END
ELSE
IF @sex = 'w'
WHILE @amount > 0
    BEGIN
	 SET @name = (SELECT TOP 1 [name] FROM [name_list_identity]
	                                  WHERE [name_list_identity].[sex] = 'w'
                                      ORDER BY NEWID())
   SET @date_of_birth = (SELECT DATEADD (DAY, RAND() * DATEDIFF(DAY, @DateStart, @DateEnd), @DateStart))
   SET @id_area = (SELECT ROUND((((SELECT COUNT(*) FROM [area_of_residence])-1)*RAND()+1),0))
   SET @id_language = (SELECT ROUND((((SELECT COUNT(*) FROM [native_language])-1)*RAND()+1),0))
   SET @married = CASE WHEN (SELECT ROUND((1*RAND()+1),0))=1 THEN 'yes' ELSE 'no'
   END
      INSERT INTO [people] ([surname], [name], [sex], [date_of_birth], [married], [id_area], [id_language]) VALUES
	                        (@surname, @name, 'w', @date_of_birth, @married, @id_area, @id_language)
	 SET @amount -=1
	 END
FETCH NEXT FROM [crs_insert_people] INTO @surname, @sex, @amount
END
CLOSE [crs_insert_people]
DEALLOCATE [crs_insert_people]
GO 
  

