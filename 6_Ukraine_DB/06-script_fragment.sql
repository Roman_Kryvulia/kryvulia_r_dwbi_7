USE [people_ua_db]
GO

CREATE UNIQUE INDEX [UQ_people_index]
ON [people] ([surname],[name], [date_of_birth])
GO


CREATE VIEW [vw_ind_stats] AS
SELECT OBJECT_NAME(A.[object_id]) AS 'tableName', 
B.[name] AS 'indexName', A.[avg_fragmentation_in_percent]
FROM  sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, 'SAMPLED') A 
INNER JOIN sys.indexes B  
ON  A.[object_id] = B.[object_id] and A. [index_id] = B. [index_id] 
GO
	
SELECT * FROM [vw_ind_stats]
 
--ALTER INDEX [PK_area_of_residence]  ON [area_of_residence]  REORGANIZE 
--ALTER INDEX [PK_name_list_identity]  ON [name_list_identity]  REORGANIZE
--GO 


DECLARE [cr_analysis_fragm] CURSOR FOR
SELECT [tableName], [indexName], [avg_fragmentation_in_percent] FROM [vw_ind_stats]
GO

OPEN [cr_analysis_fragm]

DECLARE @tab NVARCHAR(50)
DECLARE @ind NVARCHAR(50)
DECLARE @per INT
DECLARE @alt NVARCHAR(max)

FETCH NEXT FROM [cr_analysis_fragm] INTO @tab, @ind, @per
WHILE @@FETCH_STATUS = 0
BEGIN
 IF @per > 30
 BEGIN
	SET @alt = 'ALTER INDEX '+ @ind+ ' ON ' +@tab +' REBUILD WITH (FILLFACTOR = 80, SORT_IN_TEMPDB = ON, ONLINE=ON)' 
	EXEC (@alt)
 END
ELSE
IF(@per BETWEEN 5 AND 30)
  BEGIN
	SET @alt = 'ALTER INDEX '+ @ind+ ' ON ' +@tab +' REORGANIZE' 
	EXEC (@alt)
 END
FETCH NEXT FROM [cr_analysis_fragm] INTO @tab, @ind, @per
END
GO
CLOSE [cr_analysis_fragm]
DEALLOCATE [cr_analysis_fragm]
GO

SELECT * FROM [vw_ind_stats]
