USE [master]
GO

DROP DATABASE IF EXISTS [people_ua_db]
GO
CREATE DATABASE [people_ua_db]
GO
USE [people_ua_db]
GO

DROP TABLE IF EXISTS [people]
GO
DROP TABLE IF EXISTS [area_of_residence]
GO
DROP TABLE IF EXISTS [region]
GO
DROP TABLE IF EXISTS [country]
GO
DROP TABLE IF EXISTS [native_language]
GO

CREATE TABLE [native_language] (
[id_language] INT IDENTITY (1,1) PRIMARY KEY,
[native_language] NVARCHAR(20) NOT NULL
)
GO

CREATE TABLE [country] (
[id_country] INT IDENTITY (1,1) PRIMARY KEY,
[country] NVARCHAR(60) NOT NULL
)
GO

CREATE TABLE [region] (
[id_region] INT  PRIMARY KEY,
[id_country] INT NOT NULL  DEFAULT '1',
[region] NVARCHAR(60) NOT NULL,
CONSTRAINT [FK_region_country] FOREIGN KEY (id_country)
REFERENCES [country](id_country)
)
GO

CREATE TABLE [area_of_residence] (
[id_area] INT,
[id_region] INT NOT NULL,
[area] NVARCHAR(150) NOT NULL,
CONSTRAINT [PK_area_of_residence] PRIMARY KEY (id_area),
CONSTRAINT [FK_area_region] FOREIGN KEY (id_region)
REFERENCES [region](id_region)
)
GO

CREATE TABLE [people] (
[id] INT IDENTITY (1,1) PRIMARY KEY,
[surname] NVARCHAR(20) NOT NULL,
[name] NVARCHAR(20) NOT NULL,
[sex] NCHAR(1) NOT NULL,
[married] NVARCHAR(3) NOT NULL,
[date_of_birth] DATE NOT NULL,
[id_area] INT NOT NULL,
[id_language] INT NOT NULL,
CONSTRAINT [CH_sex] CHECK ([sex] IN ('m', 'w')),
CONSTRAINT [CH_married] CHECK ([married] IN ('yes', 'no')),
CONSTRAINT [FK_language_people] FOREIGN KEY (id_language)
REFERENCES [native_language](id_language),
CONSTRAINT [FK_area_people] FOREIGN KEY (id_area)
REFERENCES [area_of_residence](id_area)
)
GO

