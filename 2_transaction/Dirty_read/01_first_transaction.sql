--first transaction

USE [R_K_module_2]
GO

BEGIN TRANSACTION
UPDATE [hotel]
SET [status] = 'reserved'
WHERE [#room] = 201
GO

WAITFOR DELAY '00:00:10'
GO
ROLLBACK TRANSACTION
GO

SELECT [status] 
FROM [hotel]
WHERE [#room] = 201
GO