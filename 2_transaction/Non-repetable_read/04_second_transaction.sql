USE [R_K_module_2]
GO

BEGIN TRANSACTION

UPDATE [hotel]
SET [status] = 'reserved'
WHERE [#room] LIKE '2%';
GO

COMMIT TRANSACTION
GO