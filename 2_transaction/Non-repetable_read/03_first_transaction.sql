USE [R_K_module_2]
GO

BEGIN TRANSACTION
SELECT [#room],[status]
FROM [hotel]
WHERE [status] = 'reserved'
GO

WAITFOR DELAY '00:00:10';
GO

SELECT [#room],[status]
FROM [hotel]
WHERE [status] = 'reserved'
GO

COMMIT TRANSACTION
GO