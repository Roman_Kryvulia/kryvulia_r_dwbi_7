USE [master]
GO
DROP DATABASE IF EXISTS [R_K_module_2]
GO
CREATE DATABASE [R_K_module_2]
GO
USE [R_K_module_2]
GO


CREATE TABLE [hotel] (Id INT IDENTITY, [#room] INT, [status] VARCHAR(10))
GO
INSERT INTO [hotel] (#room, status) VALUES 
 (201, 'free'),
 (205, 'free'),
 (407, 'reserved'),
 (309, 'free')
 GO
SELECT * FROM [hotel]
