USE [master]
GO
USE [triger_fk_cursor]
GO

--creating trigger for [medicine]
CREATE TRIGGER [tr_ministry_code] ON [medicine]
AFTER  UPDATE, INSERT
AS
IF NOT EXISTS 
(SELECT * FROM inserted
WHERE [ministry_code] LIKE '[A-LNOQ-Z][A-LNOQ-Z]-[0-9][0-9][0-9]-[0-9][0-9]')
BEGIN
    PRINT 'Error, ministry code should be in the next format: 
	[letter letter - number number number - number number],
	and can not include letters [M] or [P]'
    ROLLBACK TRANSACTION
END
GO

--checking trigger
--unsuccesful insert
INSERT INTO [medicine] (name, ministry_code, recipe, narcotic, psychotropic)      
 VALUES 
 ('tramadol','ZP-2Q6-22', 1,1,1)
 GO
        --changed [P] on [A]
 INSERT INTO [medicine] (name, ministry_code, recipe, narcotic, psychotropic)      
 VALUES 
 ('tramadol','ZA-2Q6-22', 1,1,1)
 GO
 --succesful insert (changed [Q] on [0])
 INSERT INTO [medicine] (name, ministry_code, recipe, narcotic, psychotropic)      
 VALUES 
 ('tramadol','ZA-206-22', 1,1,1)
 GO
 --unsuccesful update
UPDATE [medicine]
SET [ministry_code] = 'ME-177-24'
WHERE [name] = 'tramadol'
 --succesful insert (changed [M] on [D])
 UPDATE [medicine]
SET [ministry_code] = 'DE-177-24'
WHERE [name] = 'tramadol'


--SELECT * FROM [medicine]

