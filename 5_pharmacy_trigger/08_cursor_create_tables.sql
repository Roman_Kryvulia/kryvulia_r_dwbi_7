USE [master]
GO
USE [triger_fk_cursor]
GO
--creating cursor for unique names [employee]
DECLARE @name varchar(30)
DECLARE @random_number int
DECLARE @create_table nvarchar(max)
DECLARE @add_column nvarchar(max)
DECLARE @column_type nvarchar(30)

DECLARE [crs_create_table_single_name] CURSOR 
FOR (SELECT [name] FROM [employee]
GROUP BY [name] HAVING COUNT([name])=1)

OPEN [crs_create_table_single_name]
FETCH NEXT FROM [crs_create_table_single_name] INTO @name
WHILE @@FETCH_STATUS =0
BEGIN
       SET @random_number = (SELECT ROUND((8*RAND()+1),0))
	   SET @column_type =
	   CASE
          WHEN @random_number = 1 THEN '  int,'
		  WHEN @random_number = 2 THEN '  varchar(25),'
		  WHEN @random_number = 3 THEN '  date,'
		  WHEN @random_number = 5 THEN '  bit,'
		  ELSE '  varchar(30),'
    END
  SET @create_table = 'CREATE TABLE ' + @name + ' ('
  SET @add_column =''
  WHILE @random_number > 0
 BEGIN
   SET @add_column = @add_column  +'column_'+ convert(varchar,@random_number) + @column_type 
   SET @random_number =  @random_number -1
 END
   SET @add_column = SUBSTRING(@add_column, 1, LEN(@add_column) -1) + ' )' 
   SET @create_table =@create_table + @add_column
   EXECUTE (@create_table)
   FETCH NEXT FROM [crs_create_table_single_name]  INTO  @name
END

CLOSE [crs_create_table_single_name]
DEALLOCATE [crs_create_table_single_name]
GO


--creating cursor for repeated names [employee]
DECLARE @name varchar(30)
DECLARE @surname varchar(30)
DECLARE @random_number int
DECLARE @create_table nvarchar(max)
DECLARE @add_column nvarchar(max)
DECLARE @column_type nvarchar(30)

 
DECLARE [crs_create_table_same_names] CURSOR 
FOR 
(SELECT  [employee].[name],[employee].[surname] FROM [employee]
JOIN (SELECT [name] FROM [employee]
GROUP BY [name]
HAVING COUNT([name])>1) AS [sun]
ON [employee].[name] = [sun].[name])

OPEN [crs_create_table_same_names]
FETCH NEXT FROM [crs_create_table_same_names] INTO @name, @surname
WHILE @@FETCH_STATUS =0
BEGIN
       SET @random_number = (SELECT ROUND((8*RAND()+1),0))
	   SET @column_type =
	   CASE
          WHEN @random_number = 1 THEN '  int,'
		  WHEN @random_number = 2 THEN '  varchar(25),'
		  WHEN @random_number = 3 THEN '  date,'
		  WHEN @random_number = 5 THEN '  bit,'
		  ELSE '  varchar(30),'
    END
  SET @create_table = 'CREATE TABLE ' +'['+RTRIM(@name)+'_'+@surname+']'+ ' ('
  SET @add_column =''
  WHILE @random_number > 0
 BEGIN
   SET @add_column = @add_column  +'column_'+ convert(varchar,@random_number) + @column_type 
   SET @random_number =  @random_number -1
 END
   SET @add_column = SUBSTRING(@add_column, 1, LEN(@add_column) -1) + ' )' 
   SET @create_table =@create_table + @add_column
   EXECUTE (@create_table)
   FETCH NEXT FROM [crs_create_table_same_names]  INTO  @name,@surname
END
CLOSE [crs_create_table_same_names]
DEALLOCATE [crs_create_table_same_names]
GO