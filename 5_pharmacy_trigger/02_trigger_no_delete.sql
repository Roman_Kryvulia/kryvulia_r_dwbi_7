USE [master]
GO
USE [triger_fk_cursor]
GO

--creating trigger for [street]
CREATE TRIGGER [tr_no_delete_street] ON [street]
FOR DELETE
AS
    DECLARE @street VARCHAR(25)
    SELECT @street = d.street FROM deleted d, pharmacy ph WHERE ph.street = d.street
    IF EXISTS 
	(SELECT *  FROM pharmacy WHERE street = @street)
BEGIN
    PRINT 'Error delete, you cannot delete these street (it using in table [pharmacy])'
    ROLLBACK TRANSACTION
END
GO

 --checking the trigger 
--SELECT * FROM [street]
--SELECT * FROM [pharmacy]
   -- unsuccessful delete
DELETE FROM [street]
WHERE [street] = 'Dragana'
GO
    -- successful delete ([zubrivska] doesn't in table [pharmacy])
DELETE FROM [street]
WHERE [street] = 'Zubrivska'
GO


--creating trigger for [pharmacy]
CREATE TRIGGER [tr_no_delete_pharmacy_id] ON [pharmacy]
FOR DELETE
AS
    DECLARE @ph_id INT
    SELECT @ph_id = d.id FROM deleted d, pharmacy_medicine ph_md
    WHERE ph_md.pharmacy_id = d.id
    IF EXISTS 
	(SELECT *  FROM pharmacy_medicine WHERE pharmacy_id = @ph_id)
 BEGIN
    PRINT 'Error delete, you cannot delete pharmacy_id (it using in table [pharmacy_medicine])'
    ROLLBACK TRANSACTION
 END
 GO
    
  --checking the trigger 
--SELECT * FROM [pharmacy_medicine]
--SELECT * FROM [pharmacy]

 -- unsuccessful delete
DELETE FROM [pharmacy]
WHERE [id] = 7
GO
   -- successful delete (changed id on '8' (it doesn't using in the table [pharmacy_medicine])
DELETE FROM [pharmacy]
WHERE [id] = 8
GO

--creating trigger for [medicine]
CREATE TRIGGER [tr_no_delete_medicine_id] ON [medicine]
FOR DELETE
AS
    DECLARE @med_id INT
    SELECT @med_id = d.id
    FROM deleted d, pharmacy_medicine ph_md, medicine_zone med_zn
    WHERE d.id = ph_md.medicine_id  
	   OR d.id =med_zn.medicine_id
    IF EXISTS (SELECT *  FROM pharmacy_medicine,  medicine_zone
       WHERE @med_id=pharmacy_medicine.medicine_id
	      OR @med_id=medicine_zone.medicine_id)
 BEGIN
       PRINT 'Error delete, you cannot delete medicine_id 
	   (it using in table [pharmacy_medicine] or table [medidcine_zone])'
       ROLLBACK TRANSACTION
 END
 GO
    
  --checking the trigger 
--SELECT * FROM [medicine]
--SELECT * FROM [pharmacy_medicine]
--SELECT * FROM [medicine_zone]

 -- unsuccessful delete
DELETE FROM [medicine]
WHERE [id] = 5
GO
   -- successful delete (it doesn't using in table [pharmacy_medicine] or table [medidcine_zone])
DELETE FROM [medicine]
WHERE [id] = 9
GO

--creating trigger for [zone]
CREATE TRIGGER [tr_no_delete_zone_id] ON [zone]
FOR DELETE
AS
    DECLARE @zn_id INT
    SELECT @zn_id = d.id FROM deleted d, medicine_zone md_zn
    WHERE md_zn.zone_id = d.id
    IF EXISTS
	(SELECT *  FROM medicine_zone WHERE zone_id = @zn_id)
BEGIN
    PRINT 'Error delete, you cannot delete zone_id (it table [medidcine_zone]) '
    ROLLBACK TRANSACTION
END
GO
    
  --checking the trigger 
--SELECT * FROM [medicine_zone]
--SELECT * FROM [zone]

 -- unsuccessful delete
DELETE FROM [zone]
WHERE [id] = 3
GO
 
 -- successful delete ([heart] doesn't using in table [medicine_zone])
DELETE FROM [zone]
WHERE [name] = 'heart'
GO