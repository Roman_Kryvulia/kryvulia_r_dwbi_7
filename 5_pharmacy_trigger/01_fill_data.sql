USE [master]
GO
USE [triger_fk_cursor]
GO

INSERT INTO [street] VALUES 
('Chervonoyi kalyny'), ('Sykhivska'), ('Kavaleridze'), ('Zubrivska'),
('Dragana'), ('Khutorivka'), ('Dovzhenka'), ('Kulparkivska'), ('Topolna')
GO
SELECT * FROM [street]


INSERT INTO [pharmacy] (name, building_number, www, work_time, saturday, sunday, street) VALUES
('Podorozhnyk', '97', 'www.podorozhnyk.com', '08:00:00',  1, 1, 'Chervonoyi kalyny'),
('Znakhar', '8', 'www.apteka-znahar.com.ua','08:00:00',  1, 0, 'Sykhivska'),
('DS', '36', 'www.apteka-ds.com.ua', '08:00:00', 1, 1, 'Chervonoyi kalyny'),
('Puls', '1', 'www.apteka-puls.com', '08:00:00', 1, 1, 'Dovzhenka'),
('Simejna', '2', 'www.1sa.com.ua','12:00:00',  1, 1, 'Kavaleridze'),
('3i', '1', 'www.3i.ua','08:00:00',  1, 0, 'Dragana'),
('Apteka_#266', '24', 'www.apteka-266.com.ua','08:00:00', 1, 0, 'Khutorivka'),
('Vesela apteka', '98', 'www.xi-xi.com', '08:00:00',  1, 1, 'kulparkivska'),
('Smischni ciny', '50', 'www.aliexpress.com', '08:00:00',  1, 1, 'topolna')
GO
SELECT * FROM [pharmacy]

INSERT INTO [post] VALUES
('director'), ('pharmacist'), ('driver'), ('cleaner')
GO
SELECT * FROM [post]

INSERT INTO [employee] (surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)      
 VALUES 
 ('kovalenko', 'ostap', 'petrovych', '2902908524', 'KC142251', '15.5', '1973-06-12','director', 1),
 ('tymoshyk', 'andriy', 'stepanovych', '2602901321', 'KA222474', '20.5', '1957-07-21','director', 2),
 ('mazur', 'tamara', 'dmytrivna', '2932918672', 'KB143549', '11', '1976-11-23','director', 3),
 ('svystun', 'oksana', 'semenivna', '2904602527', 'KC173314', '17.6', '1977-01-09','director', 4),
 ('snajder', 'borys', 'samoeliovych', '2612703568', 'KK146613', '25', '1955-03-19','director', 5),
 ('suprun', 'anastasiya', 'oleksandrivna', '2972438839', 'KC242711', '11.9', '1978-09-30','director', 6),
 ('korzun', 'taras', 'romanovych', '2902803577', 'KA352713', '16.4', '1975-04-22','director', 7),
 ('troyan', 'hanna', 'olehivna', '2913692467', 'KC142333', '5.3', '1982-11-11','pharmacist', 1),
 ('panas', 'oleh', 'ruslanovych', '2952704334', 'KA132558', '8.8', '1987-01-02','pharmacist', 2),
 ('ivaniv', 'jaroslav', 'stepanovych', '2888903373', 'KK145559', '7.6', '1979-06-11','pharmacist', 3),
 ('kostiv', 'maryna', 'fedorivna', '2903208565', 'KC252259', '6.6', '1980-04-24','pharmacist', 4),
 ('malevych', 'zhanna', 'pavlivna', '2992978522', 'KC132658', '4.1', '1977-09-13','pharmacist', 5),
 ('rybak', 'ihor', 'stanislavovych', '2907908727', 'KB352359', '8.2', '1967-12-30','pharmacist', 6),
 ('tepla', 'iryna', 'henadijevna', '2972904523', 'KB343244', '9', '1981-09-22','pharmacist', 7),
 ('kos', 'mykola', 'mykolajovych', '2553208584', 'KH252339', '2.4', '1985-05-26','driver', 1),
 ('marko', 'halyna', 'pavlivna', '2662978662', 'KH132258', '1', '1987-05-14','cleaner', 2),
 ('leskiv', 'volodymyr', 'volodymyrovych', '2337908732', 'KP351159', '3.5', '1957-12-31','driver', 3),
 ('udud', 'natalia', 'bohdanivna', '2992904527', 'KP343554', '4', '1991-11-21','cleaner', 4),
 ('dovhalskyi', 'ihor', 'konstiantynovych', '2507668729', 'KK357357', '3.6', '1969-10-25','driver', 5),
 ('chechur', 'daryna', 'ostapivna', '2932907542', 'KA373248', '2', '1971-05-12','cleaner', 6),
 ('krasko', 'oleh', 'mykhajlovych', '2966908788', 'KA342366', '4.5', '1965-12-04','driver', 7),
 ('filevych', 'oksana', 'pylypivna', '2971903527', 'KP243254', '2.2', '1977-03-22','cleaner', 7),
 ('rabynovych', 'josyp', 'jakovych', '3432908775', 'KH442673', '35.1', '1946-03-07','pharmacist', 9)
GO

SELECT * FROM [employee]

INSERT INTO [medicine] (name, ministry_code, recipe, narcotic, psychotropic)      
 VALUES 
 ('adaptol','N06BX21', 0,0,0),
 ('asilekt','N04BD02', 1,1,0),
 ('arlevert','N07CA52', 1,0,1),
 ('infestol','B05BA10', 0,0,0),
 ('manilin','A10BB01', 1,0,1),
 ('ranexa','C01EB18', 0,0,0),
 ('zibor','B01AB12', 1,0,0),
 ('siofor','A10BA02', 1,0,0),
  ('tramadol','N16XX22', 1,1,1)
 GO
 SELECT * FROM [medicine]
 SELECT * FROM [pharmacy]
 INSERT INTO [zone] (name)      
 VALUES
 ('head'),
 ('throat'),
 ('eyes'),
 ('teeth'),
 ('stomach'),
 ('heart')
 GO
 SELECT * FROM [medicine]
 SELECT * FROM [zone]

 INSERT INTO [pharmacy_medicine] (pharmacy_id, medicine_id)      
 VALUES 
 (1,2),(1,3),
 (2,1),(2,4),
 (3,1),(3,8),
 (4,4),(4,5),(4,1),
 (5,3),(5,7),
 (6,2),(6,6),
 (7,8),(7,5)
 GO
 SELECT * FROM [pharmacy_medicine]

 INSERT INTO [medicine_zone] (medicine_id, zone_id)      
 VALUES 
 (1,1),(1,4),
 (2,2),(2,4),
 (3,1),
 (4,5),
 (5,2), (5,3),
 (6,2),
 (7,5),
 (8,1),(8,4)
  GO
 SELECT * FROM [medicine_zone]