USE [master]
GO
USE [triger_fk_cursor]
GO
--creating trigger no_insert for [employee].[post]
CREATE TRIGGER [trg_no_insert_post] ON [employee]
FOR INSERT
AS
    DECLARE @post VARCHAR(15)
    SELECT @post = i.post
    FROM inserted i, post p
    WHERE p.post = i.post
    IF NOT EXISTS (SELECT *  FROM post 
       WHERE post = @post)
       BEGIN
       PRINT 'Error insert, you cannot insert post'
       ROLLBACK TRANSACTION
 END
 GO
  
  --checking the trigger
--SELECT * FROM [post]
-- unsuccessful insert
INSERT INTO [employee]
(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)      
 VALUES 
('pavlenko', 'ivan', 'vasyljovych', '2132958527', 'KP342579', '5.6', '1985-02-25','manager', 6)
GO
-- successful insert (changed 'manager' on 'driver'-it exists in table [post])
INSERT INTO [employee]
(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)      
 VALUES 
('pavlenko', 'ivan', 'vasyljovych', '2132958527', 'KP342579', '5.6', '1985-02-25','driver', 6)
GO

--creating trigger no_insert for [employee].[pharmacy_id]
CREATE TRIGGER [trg_no_insert_pharmacy_id] ON [employee]
FOR INSERT
AS
    DECLARE @ph_id INT
	SELECT @ph_id = i.pharmacy_id  FROM inserted i, pharmacy ph
    WHERE ph.id = i.pharmacy_id 
IF  NOT EXISTS
   (SELECT *  FROM inserted WHERE pharmacy_id IS NULL
	                           OR pharmacy_id IN (SELECT id FROM pharmacy))
  BEGIN
     PRINT 'Error insert, you cannot insert pharmacy_id'
     ROLLBACK TRANSACTION
  END
  GO
 --checking the trigger
--SELECT * FROM [pharmacy]
-- unsuccessful insert (I insert not existing pharmacy_id)
INSERT INTO [employee] 
(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)      
 VALUES 
('ostapenko', 'oksana', 'tarasivna', '2162978932', 'KK143782', '3.9', '1990-11-27','cleaner', 10)
GO
-- successful insert (changed pharmacy_id '10' on '5' and 'null')
INSERT INTO [employee]
(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)      
 VALUES 
('ostapenko', 'oksana', 'tarasivna', '2162978932', 'KK143782', '3.9', '1990-11-27','cleaner', 5)
GO
INSERT INTO [employee]
(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)      
 VALUES 
('tokalenko', 'svitlana', 'pylypivna', '2764978967', 'KK223794', '2.1', '1966-07-17','cleaner', NULL)
GO

--creating trigger no_insert_update for [pharmacy].[street]
CREATE TRIGGER [trg_no_insert_street] ON [pharmacy]
FOR INSERT, UPDATE
AS
    DECLARE @str VARCHAR(25)
	SELECT @str = i.street 
    FROM inserted i, street s
    WHERE s.street = i.street 
IF  NOT EXISTS
   (SELECT *  FROM inserted WHERE street IS NULL
	                           OR street IN (SELECT street FROM street))
  BEGIN
     PRINT 'Error insert, you cannot insert street'
     ROLLBACK TRANSACTION
  END
  GO
 --checking the trigger
--SELECT * FROM [street]
-- unsuccessful insert
INSERT INTO [pharmacy] (name, building_number, www, work_time, saturday, sunday, street) VALUES
('DS', '85', 'www.apteka-ds.com.ua', '08:00:00',  1, 1, 'Kryvorizka')
GO
-- successful insert (changed street 'Kryvorizka' on 'Chervonoyi kalyny')
INSERT INTO [pharmacy] (name, building_number, www, work_time, saturday, sunday, street) VALUES
('DS', '85', 'www.apteka-ds.com.ua', '08:00:00',  1, 1, 'Chervonoyi kalyny')
GO

--creating trigger no_insert_update for [pharmacy_medicine].[pharmacy_id]
CREATE TRIGGER [trg_no_insert_pharmacy_id_add_table] ON [pharmacy_medicine]
FOR INSERT, UPDATE
AS
    DECLARE @ph_id INT
    SELECT @ph_id = i.pharmacy_id
    FROM inserted i, pharmacy p
    WHERE p.id = i.pharmacy_id
    IF NOT EXISTS (SELECT *  FROM pharmacy
       WHERE id = @ph_id)
 BEGIN
       PRINT 'Error insert, you cannot insert_update pharmacy_id'
       ROLLBACK TRANSACTION
 END
 GO
  
  --checking the trigger
--SELECT * FROM [pharmacy]
-- unsuccessful insert
INSERT INTO [pharmacy_medicine] (pharmacy_id, medicine_id)      
 VALUES (55,2)
 GO
 -- successful insert (changed pharmacy_id '55' on '5')
 INSERT INTO [pharmacy_medicine] (pharmacy_id, medicine_id)      
 VALUES (5,2)
 GO

 --creating trigger no_insert_update for [pharmacy_medicine].[medicine_id]
CREATE TRIGGER [trg_no_insert_update_medicine_id_add_table] ON [pharmacy_medicine]
FOR INSERT, UPDATE
AS
    DECLARE @md_id INT
    SELECT @md_id = i.medicine_id
    FROM inserted i, medicine m
    WHERE m.id = i.medicine_id
    IF NOT EXISTS (SELECT *  FROM medicine
       WHERE id = @md_id)
 BEGIN
       PRINT 'Error insert, you cannot insert_update medicine_id'
       ROLLBACK TRANSACTION
 END
  GO
  --checking the trigger
--SELECT * FROM [medicine]
-- unsuccessful insert
INSERT INTO [pharmacy_medicine] (pharmacy_id, medicine_id)      
 VALUES (5,22)
 GO
 -- successful insert (changed medicine_id '22' on '6')
 INSERT INTO [pharmacy_medicine] (pharmacy_id, medicine_id)      
 VALUES (5,6)
 GO


  --creating trigger no_insert_update for [medicine_zone].[medicine_id]
CREATE TRIGGER [trg_no_insert_update_medicine_id_add_table#2] ON [medicine_zone]
FOR INSERT, UPDATE
AS
    DECLARE @md_id INT
    SELECT @md_id = i.medicine_id
    FROM inserted i, medicine m
    WHERE m.id = i.medicine_id
    IF NOT EXISTS (SELECT *  FROM medicine
       WHERE id = @md_id)
 BEGIN
       PRINT 'Error insert, you cannot insert_update medicine_id'
       ROLLBACK TRANSACTION
 END
 GO
  
  --checking the trigger
--SELECT * FROM [medicine]
-- unsuccessful insert
 INSERT INTO [medicine_zone] (medicine_id, zone_id)      
 VALUES (10,1)
 GO
 -- successful insert (changed medicine_id '10' on '2')
  INSERT INTO [medicine_zone] (medicine_id, zone_id)      
 VALUES (2,1)
 GO

  --creating trigger no_insert_update for [medicine_zone].[zone_id]
CREATE TRIGGER [trg_no_insert_update_zone_id_add_table#2] ON [medicine_zone]
FOR INSERT, UPDATE
AS
    DECLARE @zn_id INT
    SELECT @zn_id = i.zone_id
    FROM inserted i, [zone] z
    WHERE z.id = i.zone_id
    IF NOT EXISTS (SELECT *  FROM [zone]
       WHERE id = @zn_id)
 BEGIN
       PRINT 'Error insert, you cannot insert_update zone_id'
       ROLLBACK TRANSACTION
 END
  GO
  --checking the trigger
--SELECT * FROM [zone]
-- unsuccessful insert
INSERT INTO [medicine_zone] (medicine_id, zone_id)      
 VALUES (3,9)
 GO
 -- successful insert (changed medicine_id '9' on '2')
INSERT INTO [medicine_zone] (medicine_id, zone_id)      
 VALUES (3,2)
 GO

