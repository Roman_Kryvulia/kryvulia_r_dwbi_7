--creating cursor for unique names [employee]

USE [triger_fk_cursor]
GO

DECLARE @id INT
DECLARE @surname VARCHAR(30) 
DECLARE @name CHAR(30)
DECLARE @midle_name VARCHAR(30)
DECLARE @identity_number CHAR(10)
DECLARE @passport CHAR(10)
DECLARE @experience DECIMAL(10, 1)
DECLARE @birthday DATE
DECLARE @post VARCHAR(15)
DECLARE @pharmacy_id INT

DECLARE @cre_tab_ins_rec nvarchar(max)
DECLARE @name_table nvarchar(max)
DECLARE @A_name_table nvarchar(max)
DECLARE @B_name_table nvarchar(max)
DECLARE @time DATETIME



DECLARE [crs_insert_records] CURSOR FOR
(SELECT [id], [surname], [name], [midle_name], [identity_number], [passport], [experience], [birthday], [post], [pharmacy_id] 
FROM [employee]
WHERE [name] IN  (SELECT [name] FROM [employee]
GROUP BY [name] HAVING COUNT([name])=1))

OPEN [crs_insert_records]

FETCH NEXT FROM [crs_insert_records] INTO @id, @surname,@name, @midle_name, @identity_number, @passport, @experience,@birthday, @post, @pharmacy_id 
WHILE @@FETCH_STATUS = 0

BEGIN
     
   BEGIN
   SET @time = (SELECT getdate())
   SET @A_name_table = '['+RTRIM(@name)+'_A_'+(CONVERT (varchar, @time,114))+']'
   SET @B_name_table = '['+RTRIM(@name)+'_B_'+(CONVERT (varchar, @time,114))+']'
   
   SET @name_table =
   CASE
   WHEN (SELECT ROUND((1*RAND()+1),0))=1 THEN @A_name_table ELSE @B_name_table
   END

   SET @cre_tab_ins_rec = 'CREATE TABLE '+@A_name_table+' (
    id                 INT,
    surname            VARCHAR(30),
    name               CHAR(30),
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
    experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15),
    pharmacy_id        INT
   
)'+
'CREATE TABLE '+@B_name_table+' (
    id                 INT,
    surname            VARCHAR(30),
    name               CHAR(30),
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
    experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15),
    pharmacy_id        INT
)'+'INSERT INTO '+@name_table+'SELECT * FROM [employee]  WHERE [employee].[name] = '''+@name+''' '

  END
 
EXECUTE (@cre_tab_ins_rec)
FETCH NEXT FROM [crs_insert_records] INTO @id, @surname,@name, @midle_name, @identity_number, @passport, @experience,@birthday, @post, @pharmacy_id 
END
CLOSE [crs_insert_records]
DEALLOCATE [crs_insert_records]
GO


--creating cursor for repeated names [employee]
USE [triger_fk_cursor]
GO

DECLARE @id INT
DECLARE @surname VARCHAR(30) 
DECLARE @name CHAR(30)
DECLARE @midle_name VARCHAR(30)
DECLARE @identity_number CHAR(10)
DECLARE @passport CHAR(10)
DECLARE @experience DECIMAL(10, 1)
DECLARE @birthday DATE
DECLARE @post VARCHAR(15)
DECLARE @pharmacy_id INT

DECLARE @cre_tab_ins_rec nvarchar(max)
DECLARE @name_table nvarchar(max)
DECLARE @A_name_table nvarchar(max)
DECLARE @B_name_table nvarchar(max)
DECLARE @time DATETIME



DECLARE [crs_insert_records] CURSOR FOR
(SELECT [id], [surname], [name], [midle_name], [identity_number], [passport], [experience], [birthday], [post], [pharmacy_id] 
FROM [employee]
WHERE [name] IN  (SELECT [name] FROM [employee]
GROUP BY [name] HAVING COUNT([name])>1))

OPEN [crs_insert_records]

FETCH NEXT FROM [crs_insert_records] INTO @id, @surname,@name, @midle_name, @identity_number, @passport, @experience,@birthday, @post, @pharmacy_id 
WHILE @@FETCH_STATUS = 0

BEGIN
     
   BEGIN
   SET @time = (SELECT getdate())
   SET @A_name_table = '['+RTRIM(@name)+'_'+RTRIM(@surname)+'_A_'+(CONVERT (varchar, @time,114))+']'
   SET @B_name_table = '['+RTRIM(@name)+'_'+RTRIM(@surname)+'_B_'+(CONVERT (varchar, @time,114))+']'
   
   SET @name_table =
   CASE
   WHEN (SELECT ROUND((1*RAND()+1),0))=1 THEN @A_name_table ELSE @B_name_table
   END

   SET @cre_tab_ins_rec = 'CREATE TABLE '+@A_name_table+' (
    id                 INT,
    surname            VARCHAR(30),
    name               CHAR(30),
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
    experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15),
    pharmacy_id        INT
   
)'+
'CREATE TABLE '+@B_name_table+' (
    id                 INT,
    surname            VARCHAR(30),
    name               CHAR(30),
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
    experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15),
    pharmacy_id        INT
)'+'INSERT INTO '+@name_table+'SELECT * FROM [employee]  WHERE [employee].[surname] = '''+@surname+''' '

  END
 
EXECUTE (@cre_tab_ins_rec)
FETCH NEXT FROM [crs_insert_records] INTO @id, @surname,@name, @midle_name, @identity_number, @passport, @experience,@birthday, @post, @pharmacy_id 
END
CLOSE [crs_insert_records]
DEALLOCATE [crs_insert_records]
GO


