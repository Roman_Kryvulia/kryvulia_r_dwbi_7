USE [master]
GO
USE [triger_fk_cursor]
GO

--creating trigger cascade_update for [post]
CREATE TRIGGER [trg_update_post] ON [post]
AFTER UPDATE
AS
BEGIN
   UPDATE [employee]
   SET [employee].[post] = [inserted].[post] FROM [inserted]
   WHERE [employee].[post] = (SELECT [deleted].[post] FROM [deleted])
END
GO
  --checking the trigger
UPDATE [post] 
SET [post]  ='boss' 
WHERE [post]  = 'director'
GO

--SELECT * FROM [post]
--SELECT * FROM [employee]

--creating trigger cascade_set null for [pharmacy].[id]
CREATE TRIGGER [trg_delete_id] ON [pharmacy]
AFTER DELETE
AS
BEGIN
UPDATE [employee]
   SET [employee].[pharmacy_id] = NULL
   WHERE [employee].[pharmacy_id] = (SELECT [deleted].[id] FROM [deleted])
END
GO

--checking the trigger
--(delete record in [pharmacy] and insert NULL in column pharmacy_id table [employee])
DELETE FROM [pharmacy] 
WHERE [name]  = 'smischni ciny'
GO

--SELECT * FROM [pharmacy]
--SELECT * FROM [employee]

