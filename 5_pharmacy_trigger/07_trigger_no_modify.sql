USE [master]
GO
USE [triger_fk_cursor]
GO

--creating trigger for [post]
CREATE TRIGGER [tr_no_update_post] ON [post]
INSTEAD OF UPDATE, INSERT, DELETE
AS
BEGIN
    PRINT 'Error update, you cannot modify [post]'
    ROLLBACK TRANSACTION
END
GO

--checking the trigger
UPDATE [post] 
SET [post]  ='director' 
WHERE [post]  = 'boss'
GO

INSERT INTO [post] VALUES
('director')
GO

DELETE FROM [post]
WHERE [post] = 'boss' 
GO

--SELECT * FROM [post]