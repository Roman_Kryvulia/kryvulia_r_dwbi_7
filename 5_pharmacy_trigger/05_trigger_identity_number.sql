USE [master]
GO
USE [triger_fk_cursor]
GO

--creating trigger for [employee]
CREATE TRIGGER [tr_no_double_zero_in_ending_identity_number] ON [employee]
AFTER  UPDATE, INSERT
AS
IF EXISTS 
(SELECT * FROM inserted
WHERE [identity_number] LIKE'%00')
BEGIN
    PRINT 'Error, identity number should not end with double zeros'
    ROLLBACK TRANSACTION
END

--checking trigger
--unsuccessful insert(update)
UPDATE [employee] 
SET [identity_number]  ='2902908500' 
WHERE [passport]  = 'KC142251'
GO
INSERT INTO [employee] (surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)      
 VALUES 
 ('nepyipyvo', 'severyn', 'tarasovych', '3334445500', 'KC778899', '25.5', '1965-11-11','driver', 5)
 GO
 --successful update (changed identity_number)
 UPDATE [employee] 
SET [identity_number]  ='2902908588' 
WHERE [passport]  = 'KC142251'
GO


--SELECT * FROM [employee]
--SELECT * FROM [employee]
--WHERE [identity_number] LIKE'%00'
