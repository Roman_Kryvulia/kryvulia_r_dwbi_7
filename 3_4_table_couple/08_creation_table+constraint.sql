USE [master]
GO
DROP DATABASE IF EXISTS [R_K_module_4]
GO
CREATE DATABASE R_K_module_4
GO
USE R_K_module_4
GO

CREATE TABLE voucher
(
    Id INT IDENTITY PRIMARY KEY,
    firstname_guest NVARCHAR(30) NOT NULL,
	surname_guest NVARCHAR(50) NOT NULL,
	age INT NOT NULL,
	country NVARCHAR(50) NOT NULL,
    passport_number VARCHAR(50) NOT NULL,
    phone VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    name_apartmany NVARCHAR(50) NOT NULL,
    number_of_person INT NOT NULL,
    adults INT NOT NULL,
    children INT NOT NULL DEFAULT (0),
	arrival DATE NOT NULL,
    departure DATE NOT NULL,
    payment_method NVARCHAR(50) NOT NULL,
    amount_to_pay FLOAT NOT NULL,
	CONSTRAINT UQ_voucher__passport_number UNIQUE (passport_number),
    CONSTRAINT UQ_voucher__email UNIQUE (email),
)
GO

CREATE TABLE voucher_log 
(
    id INT NOT NULL,
    firstname_guest NVARCHAR(30) NOT NULL,
	surname_guest NVARCHAR(50) NOT NULL,
	age INT NOT NULL,
	country NVARCHAR(50) NOT NULL,
    passport_number VARCHAR(50) NOT NULL,
    phone VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    name_apartmany NVARCHAR(50) NOT NULL,
    number_of_person INT NOT NULL,
    adults INT NOT NULL,
    children INT NOT NULL DEFAULT (0),
	arrival DATE NOT NULL,
    departure DATE NOT NULL,
    payment_method NVARCHAR(50) NOT NULL,
    amount_to_pay FLOAT NOT NULL,
    type_of_operation CHAR(3),
    date_of_operation DATE NOT NULL DEFAULT GETDATE()
)
GO

