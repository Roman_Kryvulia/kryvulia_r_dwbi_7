USE [master]
GO
DROP DATABASE IF EXISTS [education]
GO
CREATE DATABASE [education]
GO
USE [education]
GO
CREATE SCHEMA [R_Kryvulia]
GO

CREATE SYNONYM R_Kryvulia.voucher_sn
FOR R_K_module_4.dbo.voucher  
GO 

CREATE SYNONYM R_Kryvulia.voucher_log_sn  
FOR R_K_module_4.dbo.voucher_log  
GO 


SELECT * FROM R_Kryvulia.voucher_sn 
SELECT * FROM R_Kryvulia.voucher_log_sn
GO

SELECT surname_guest, age, phone 
FROM R_Kryvulia.voucher_sn  
WHERE age > 25  
GO

SELECT id, surname_guest, country, type_of_operation 
FROM R_Kryvulia.voucher_log_sn 
WHERE type_of_operation = 'DEL'  
GO


