USE R_K_module_4
GO

CREATE TRIGGER trig_DEL
ON voucher
AFTER DELETE
AS
INSERT INTO voucher_log (Id, firstname_guest, surname_guest, age, country, passport_number, phone, email, 
name_apartmany, number_of_person, adults, children, arrival, departure, payment_method, amount_to_pay, type_of_operation)
SELECT Id, firstname_guest, surname_guest, age,country, passport_number, phone, email,
 name_apartmany, number_of_person, adults, children, arrival, departure, payment_method, amount_to_pay, 'DEL'
FROM DELETED


--checking
DELETE FROM voucher
WHERE firstname_guest = 'sonya'

SELECT * FROM voucher
SELECT * FROM voucher_log