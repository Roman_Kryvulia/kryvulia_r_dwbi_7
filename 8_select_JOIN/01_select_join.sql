USE [labor_sql]
GO
--#1
SELECT DISTINCT p.[maker], p.[type], pc.[speed], pc.[hd]  
FROM [product] p JOIN [pc]
ON p.[model] =pc.[model]
WHERE pc.[hd] <= 8
GO
--#2
SELECT DISTINCT p.[maker] 
FROM [product] p JOIN [pc]
ON p.[model] =pc.[model]
WHERE pc.[speed] > 600
GO
--#3
SELECT DISTINCT p.[maker] 
FROM [product] p JOIN [laptop] l
ON p.[model] =l.[model]
WHERE l.[speed] <= 500
GO
--#4
SELECT   l1.[model]  [model1] ,  l2.[model]  [model2], l1.[hd], l1.[ram]
FROM   [laptop] l1,  [laptop] l2
WHERE   l1.[model] = l2.[model] AND l1.[hd] = l2.[hd] AND l1.[ram] = l2.[ram] AND  l1.[code] > l2.[code]
GO
--#5
SELECT DISTINCT c1.[country], c2.[class] [type_bb], c1.[class] [type_bc]
FROM [classes] c1, [classes] c2
WHERE c2.[type] = 'bb' AND c1.[type] = 'bc' AND c2.[country] = c1.[country]
GO
--#6 
SELECT DISTINCT p.[maker],pc.[model] 
FROM [product] p JOIN [pc]
ON p.[model] =pc.[model]
WHERE pc.[price] < 600
GO
--#7
SELECT DISTINCT pr.[model], p.[maker]
FROM [product] p JOIN [printer] pr
ON p.[model] =pr.[model]
WHERE pr.[price] > 300
GO
--#8
SELECT DISTINCT p.[maker],pc.[model], pc.[price] 
FROM [product] p JOIN [pc]
ON p.[model] =pc.[model]
GO
--#9
SELECT DISTINCT p.[maker],p.[model], pc.[price] 
FROM [product] p LEFT JOIN [pc]
ON p.[model] =pc.[model]
WHERE p.[type] = 'pc'
GO
--#10
SELECT DISTINCT p.[maker], p.[type], l.[model], l.[speed] 
FROM [product] p JOIN [laptop] l
ON p.[model] =l.[model]
WHERE l.[speed] > 600
GO
--#11
SELECT s.[name], c.[displacement] 
FROM [ships] s JOIN [classes] c 
ON s.[class] = c.[class]
GO
--#12
SELECT o.[ship], b.[name], b.[date]
FROM [outcomes] o JOIN [battles] b
ON o.[battle] = b.[name]
WHERE o.[result] != 'sunk'
GO
--#13
SELECT s.[name], c.[country] 
FROM [ships] s JOIN [classes] c 
ON s.[class] = c.[class]
GO
--#14
SELECT DISTINCT t.[plane], c.[name]
FROM [trip] t JOIN [company] c
ON t.[id_comp] = c.[id_comp]
WHERE t.[plane] = 'boeing'
GO
--#15
SELECT p.[name], pt.[date]
FROM [passenger] p JOIN [pass_in_trip] pt
ON p.[id_psg]  = pt.[id_psg]
--ORDER BY p.[name]
GO
--#16
SELECT DISTINCT pc.[model], pc.[speed], pc.[hd] FROM [pc] JOIN [product] p
ON p.[model] =pc.[model]
WHERE p.[maker] = 'A' AND pc.[hd] IN (10,20)
ORDER BY pc.[speed]
GO
--#17
SELECT * FROM (SELECT [maker], [type]
              FROM [product]) AS [inf]
PIVOT(COUNT([type])
      FOR [type] IN ([pc], [laptop], [printer])) AS [pvt_t]
GO
--#18
SELECT 'average price' [avg_], [11], [12], [14], [15] FROM (SELECT [screen], [price]
              FROM [laptop]) AS [inf]
PIVOT(AVG([price])
      FOR [screen] IN ([11], [12], [14], [15])) AS [pvt_t]
GO
--#19
SELECT * 
FROM [laptop] l CROSS APPLY (SELECT [maker] FROM [product] p WHERE l.[model] = p.[model]) p
GO
--#20
SELECT *
FROM [laptop] l CROSS APPLY (SELECT  MAX(price) [max_price] FROM [laptop] l2
JOIN  [product] p ON l2.[model]=p.[model] 
WHERE p.[maker] = (SELECT [maker] FROM [product] p2 WHERE p2.[model]= l.[model])
) p
GO
 --#21
SELECT * 
FROM [laptop] l
CROSS APPLY (SELECT TOP(1)* 
FROM [laptop] l2 
WHERE l.model < l2.model OR (l.model = l2.model AND l.code < l2.code) 
ORDER BY l2.[model], l2.[code]) p
ORDER BY l.[model], l.[code]
GO
--#22
SELECT * 
FROM [laptop] l
OUTER APPLY (SELECT TOP(1)* 
FROM [laptop] l2 
WHERE l.model < l2.model OR (l.model = l2.model AND l.code < l2.code) 
ORDER BY l2.[model], l2.[code]) p
ORDER BY l.[model], l.[code]
GO
--#23
SELECT t.* FROM 
(SELECT DISTINCT [type] FROM [product]) p
CROSS APPLY 
(SELECT TOP (3)* FROM [product] p2
 WHERE  p.[type] = p2.[type]
 ORDER BY p2.[model]) t
 GO
--#24
SELECT [code], [name], [value] FROM [laptop]
CROSS APPLY
(VALUES ('speed', [speed]),('ram', [ram]), ('hd', [hd]), ('screen', [screen])
) spec ([name], [value])
WHERE [code] <= 3
ORDER BY [code], [name], [value]
GO